/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_administrar_row.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_administrar_entidades_row.ui" )]
  public class AdministrarEntidadesRow : Gtk.Grid {
    private string tabla;
    private string id;
    private string[] campos;
    [GtkChild]
    private Gtk.Label label_nombre;

    public AdministrarEntidadesRow ( string tabla, string id, string[] campos ) {
      this.tabla = tabla;
      this.id = id;
      this.campos = campos;

      this.set_label ();
    }

    private void set_label () {
      string campos_query = this.armar_campos_query ();
      string label = "";

      if ( campos_query != "" ) {
        var entidad = ApplicationWindow.db.select ( this.tabla, campos_query, "WHERE id='" + id + "'" ).index ( 0 ).split ( "|" );

        for ( int i = 0; i < campos.length; i++ ) {
          label += entidad[i];

          if ( i < ( campos.length - 1 ) ) {
            label += " ";
          }
        }
      }

      this.label_nombre.set_label ( label );
    }

    private string armar_campos_query () {
      string retorno = "";

      for ( int i = 0; i < this.campos.length; i++ ) {
        retorno += this.campos[i];

        if ( i < ( campos.length - 1 ) ) {
          retorno += ", ";
        }
      }

      return retorno;
    }

    public string get_id () {
      return this.id;
    }

    public string get_label () {
      return this.label_nombre.get_label ();
    }
  }
}
