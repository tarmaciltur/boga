/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_application_window.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_application_window.ui" )]
  public class ApplicationWindow : Gtk.ApplicationWindow {
    public static Doctrina.BaseDeDatos db;
    public static Doctrina.Infraestructura infra;
    public static int dpi;
    [GtkChild]
    private Gtk.Stack stack_principal;
    [GtkChild]
    private Gtk.Stack stack_headerbar_izquierdo;
    [GtkChild]
    private Gtk.Stack stack_headerbar_derecho;
    [GtkChild]
    private Gtk.ListStore liststore_plantillas;
    [GtkChild]
    private Gtk.ComboBox combobox_plantillas;
    [GtkChild]
    private Gtk.MenuButton menubutton_menu;
    [GtkChild]
    private Boga.Editor editor;
    [GtkChild]
    private Boga.AdministrarAbogados administrar_abogados;
    [GtkChild]
    private Boga.AdministrarDomicilios administrar_domicilios;
    [GtkChild]
    private Boga.AdministrarPlantillas administrar_plantillas;

    public ApplicationWindow ( Gtk.Application app ) {
      Object ( application: app );

      Environment.set_prgname ( "boga" );
      Environment.set_application_name ( "Boga" );
      Gtk.Window.set_default_icon_name ( "ar.com.softwareperonista.Boga" );

      ApplicationWindow.infra = new Doctrina.Infraestructura (
        "boga",
        "6",
        "resource:///ar/com/softwareperonista/boga/db/boga_base_de_datos_definicion.sql" );
      ApplicationWindow.db = new Doctrina.BaseDeDatos ( "db.sqlite",
                                                        ApplicationWindow.infra );
      ApplicationWindow.dpi = 72;

      if ( ApplicationWindow.db.select ( "plantillas", "nombre", "WHERE nombre='Solicito'" ).index ( 0 ) == null ) {
        string etag_out;
        uint8[] plantilla;
        var file_plantilla = File.new_for_uri ( "resource:///ar/com/softwareperonista/boga/plantilla/documento.boga" );

        try {
          file_plantilla.load_contents ( null, out plantilla, out etag_out );
        } catch ( GLib.Error e ) {
          stderr.printf ( "%s\n", e.message );
        }
        ApplicationWindow.db.insert ( "plantillas", "nombre,definicion", "'Solicito','" + (string)plantilla + "'" );
      }

      if ( ApplicationWindow.db.select ( "plantillas", "nombre", "WHERE nombre='Informe Denuncia Negativa'" ).index ( 0 ) == null ) {
        string etag_out;
        uint8[] plantilla;
        var file_plantilla = File.new_for_uri ( "resource:///ar/com/softwareperonista/boga/plantilla/informe_negativa.boga" );

        try {
          file_plantilla.load_contents ( null, out plantilla, out etag_out );
        } catch ( GLib.Error e ) {
          stderr.printf ( "%s\n", e.message );
        }
        ApplicationWindow.db.insert ( "plantillas", "nombre,definicion", "'Informe Denuncia Negativa','" + (string)plantilla + "'" );
      }

      this.cargar_liststore_plantillas ();
      this.setear_menu_principal ();
      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_crear_documento" );
    }

    private void setear_menu_principal () {
      var builder = new Gtk.Builder ();
      try {
        builder.add_from_resource ( "/ar/com/softwareperonista/boga/ui/boga_menu_principal.ui" );
        this.menubutton_menu.set_menu_model ( builder.get_object ( "menu-principal" ) as Menu );
      } catch (  Error e ) {
        error ( "loading ui file: %s", e.message );
      }

      var action = new SimpleAction ( "administrar_abogados", null );
      action.set_enabled ( true );
      action.activate.connect ( () => { this.abir_administrar_abogados (); } );
      this.application.add_action ( action );

      action = new SimpleAction ( "administrar_domicilios", null );
      action.set_enabled ( true );
      action.activate.connect ( () => { this.abir_administrar_domicilios (); } );
      this.application.add_action ( action );

      action = new SimpleAction ( "administrar_plantillas", null );
      action.set_enabled ( true );
      action.activate.connect ( () => { this.abir_administrar_plantillas (); } );
      this.application.add_action ( action );

      action = new SimpleAction ( "about", null );
      action.set_enabled ( true );
      action.activate.connect ( () => { this.about (); } );
      this.application.add_action ( action );

      action = new SimpleAction ( "quit", null );
      action.set_enabled ( true );
      action.activate.connect ( () => { this.quit (); } );
      this.application.add_action ( action );

    }

    private void about () {
      string[] authors = {
        "Andres Fernandez <andres@softwareperonista.com.ar>",
        "Fernando Fernandez <fernando@softwareperonista.com.ar>"
      };
      Gtk.show_about_dialog ( this,
                              "authors", authors,
                              "program-name", "Boga",
                              "title", _("About" ) + " Boga",
                              "comments", "Creador de escritos judiciales",
                              "copyright", "Copyright 2018 Fernando Fernández y Andres Fernández",
                              "license-type", Gtk.License.GPL_3_0,
                              "logo-icon-name", "ar.com.softwareperonista.Boga",
                              "version", Config.VERSION,
                              "website", "https://git.softwareperonista.com.ar/cdp/boga",
                              "wrap-license", true );
    }

    private void quit () {
      this.close ();
    }

    [GtkCallback]
    private void button_guardar_documento_clicked () {
      Gtk.TreeIter iter;
      Value value_nombre;
      this.combobox_plantillas.get_active_iter ( out iter );

      this.liststore_plantillas.get_value ( iter, 0, out value_nombre );

      string nombre = (string)value_nombre;

      string directorio = GLib.Environment.get_user_special_dir ( GLib.UserDirectory.DOCUMENTS ) + "/Boga";

      if ( !Doctrina.Infraestructura.existe_path ( directorio ) ) {
        Doctrina.Infraestructura.crear_directorio ( directorio );
      }

      this.editor.imprimir ( directorio + "/" + nombre + ".pdf" );
    }

    private void cargar_liststore_plantillas () {
      Gtk.TreeIter iter;
      this.liststore_plantillas.clear ();

      var plantillas = ApplicationWindow.db.select ( "plantillas", "nombre,id", "" );

      for ( int i = 0; i < plantillas.length; i++ ) {
        var plantilla = plantillas.index ( i ).split ( "|" );

        var nombre = plantilla[0];
        var id = plantilla[1];

        this.liststore_plantillas.append ( out iter );
        this.liststore_plantillas.set ( iter, 0, nombre, 1, id );
      }

      this.combobox_plantillas.set_active ( 0 );
    }


    [GtkCallback]
    private void combobox_plantillas_changed () {
      Gtk.TreeIter iter;
      Value value_id;
      this.combobox_plantillas.get_active_iter ( out iter );

      this.liststore_plantillas.get_value ( iter, 1, out value_id );

      string id = (string)value_id;

      var plantilla = ApplicationWindow.db.select ( "plantillas", "definicion", "WHERE id='" + id + "'" ).index ( 0 );

      this.editor.cargar_plantilla ( plantilla );
    }

    private void abir_administrar_abogados () {
      if ( this.administrar_abogados.cargar_entidades () ) {
        this.administrar_abogados.ver_entidades ();

        this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_abogados" );
        this.stack_headerbar_derecho.set_visible_child_name ( "page_principal" );
      }

      this.stack_principal.set_visible_child_name ( "page_administrar_abogados" );
    }

    [GtkCallback]
    private void administrar_abogados_sin_entidades () {
      this.button_agregar_abogado_clicked ();
    }

    [GtkCallback]
    private void button_agregar_abogado_clicked () {
      this.administrar_abogados.agregar_entidad ();

      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_abogados_aceptar" );
      this.stack_headerbar_derecho.set_visible_child_name ( "page_administrar_abogados_cancelar" );
    }

    [GtkCallback]
    private void button_administrar_abogados_aceptar_clicked () {
      if ( this.administrar_abogados.agregar_entidad_aceptar () )
      {
        this.abir_administrar_abogados ();
      }
    }

    [GtkCallback]
    private void button_administrar_abogados_cancelar_clicked () {
      if ( this.administrar_abogados.cargar_entidades () ) {
        this.abir_administrar_abogados ();
      } else {
        this.stack_headerbar_izquierdo.set_visible_child_name ( "page_crear_documento" );
        this.stack_headerbar_derecho.set_visible_child_name ( "page_principal" );
        this.stack_principal.set_visible_child_name ( "page_editor" );
      }
    }

    [GtkCallback]
    private void button_abogado_editar_clicked () {
      this.administrar_abogados.editar_entidad ();

      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_abogados_aceptar" );
      this.stack_headerbar_derecho.set_visible_child_name ( "page_administrar_abogados_cancelar" );
    }

    [GtkCallback]
    private void button_abogado_eliminar_clicked () {
      this.administrar_abogados.eliminar_entidad ();

      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_abogados_aceptar" );
      this.stack_headerbar_derecho.set_visible_child_name ( "page_administrar_abogados_cancelar" );
    }

    private void abir_administrar_domicilios () {
      if ( this.administrar_domicilios.cargar_entidades () ) {
        this.administrar_domicilios.ver_entidades ();

        this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_domicilios" );
        this.stack_headerbar_derecho.set_visible_child_name ( "page_principal" );
      }

      this.stack_principal.set_visible_child_name ( "page_administrar_domicilios" );
    }

    [GtkCallback]
    private void administrar_domicilios_sin_entidades () {
      this.button_agregar_domicilio_clicked ();
    }

    [GtkCallback]
    private void button_agregar_domicilio_clicked () {
      this.administrar_domicilios.agregar_entidad ();

      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_domicilios_aceptar" );
      this.stack_headerbar_derecho.set_visible_child_name ( "page_administrar_domicilios_cancelar" );
    }

    [GtkCallback]
    private void button_administrar_domicilios_aceptar_clicked () {
      if ( this.administrar_domicilios.agregar_entidad_aceptar () )
      {
        this.abir_administrar_domicilios ();
      }
    }

    [GtkCallback]
    private void button_administrar_domicilios_cancelar_clicked () {
      if ( this.administrar_domicilios.cargar_entidades () ) {
        this.abir_administrar_domicilios ();
      } else {
        this.stack_headerbar_izquierdo.set_visible_child_name ( "page_crear_documento" );
        this.stack_headerbar_derecho.set_visible_child_name ( "page_principal" );
        this.stack_principal.set_visible_child_name ( "page_editor" );
      }
    }

    [GtkCallback]
    private void button_domicilio_editar_clicked () {
      this.administrar_domicilios.editar_entidad ();

      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_domicilios_aceptar" );
      this.stack_headerbar_derecho.set_visible_child_name ( "page_administrar_domicilios_cancelar" );
    }

    [GtkCallback]
    private void button_domicilio_eliminar_clicked () {
      this.administrar_domicilios.eliminar_entidad ();

      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_domicilios_aceptar" );
      this.stack_headerbar_derecho.set_visible_child_name ( "page_administrar_domicilios_cancelar" );
    }

    private void abir_administrar_plantillas () {
      if ( this.administrar_plantillas.cargar_plantillas () ) {
        this.administrar_plantillas.ver_plantillas ();

        this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_plantillas" );
        this.stack_headerbar_derecho.set_visible_child_name ( "page_principal" );
      }

      this.stack_principal.set_visible_child_name ( "page_administrar_plantillas" );
    }

    [GtkCallback]
    private void administrar_plantillas_sin_plantillas () {
      this.button_agregar_plantilla_clicked ();
    }

    [GtkCallback]
    private void button_agregar_plantilla_clicked () {
      this.administrar_plantillas.agregar_plantilla ();

      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_plantillas_aceptar" );
      this.stack_headerbar_derecho.set_visible_child_name ( "page_administrar_plantillas_cancelar" );
    }

    [GtkCallback]
    private void button_administrar_plantillas_aceptar_clicked () {
      if ( this.administrar_plantillas.agregar_plantilla_aceptar () )
      {
        this.abir_administrar_plantillas ();
      }
    }

    [GtkCallback]
    private void button_administrar_plantillas_cancelar_clicked () {
      if ( this.administrar_plantillas.cargar_plantillas () ) {
        this.abir_administrar_plantillas ();
      } else {
        this.stack_headerbar_izquierdo.set_visible_child_name ( "page_crear_documento" );
        this.stack_headerbar_derecho.set_visible_child_name ( "page_principal" );
        this.stack_principal.set_visible_child_name ( "page_editor" );
      }
    }

    [GtkCallback]
    private void button_plantilla_editar_clicked () {
      this.administrar_plantillas.editar_plantilla ();

      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_plantillas_aceptar" );
      this.stack_headerbar_derecho.set_visible_child_name ( "page_administrar_plantillas_cancelar" );
    }

    [GtkCallback]
    private void button_plantilla_eliminar_clicked () {
      this.administrar_plantillas.eliminar_plantilla ();

      this.stack_headerbar_izquierdo.set_visible_child_name ( "page_administrar_plantillas_aceptar" );
      this.stack_headerbar_derecho.set_visible_child_name ( "page_administrar_plantillas_cancelar" );
    }
  }
}
