/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_ingreso_entidad.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_ingreso_entidad.ui" )]
  public class IngresoEntidad : Ingreso {
    [GtkChild]
    private Gtk.ComboBox combobox_datos;
    [GtkChild]
    private Gtk.ListStore liststore_datos;

    public IngresoEntidad ( string campo ) {
      base ( campo, "|", "|" );

      this.cargar_liststore ();
    }

    public override string get_dato () {
      Gtk.TreeIter iter;
      Value valor;
      string texto = "";
      string id = "";

      this.combobox_datos.get_active_iter ( out iter );

      if ( this.liststore_datos.iter_is_valid ( iter ) ) {
        this.liststore_datos.get_value ( iter, 1, out valor );
        id = (string)valor;
      }

      if ( id != "" ) {
        string tabla = this.get_tabla ();
        string campos_db = this.get_campos_db_datos ();

        int inicio_texto = this.get_campo ().last_index_of ( ":" ) + 1;
        int largo_texto = this.get_campo ().length - inicio_texto -1;

        texto = this.get_campo ().substring ( inicio_texto, largo_texto );

        var entidad = ApplicationWindow.db.select ( tabla, campos_db, "WHERE id = " + id ).index ( 0 ).split ( "|" );

        var campos_texto = campos_db.up ().split ( "," );
        for ( int i = 0; i < entidad.length; i++ ) {
          texto = texto.replace ( "=" + campos_texto[i] + "=", entidad[i] );
        }
      }

      return texto;
    }

    [GtkCallback]
    private void cambio () {
      this.cambio_texto ();
    }

    private string get_tabla () {
      var info_db = this.get_campo ().down ().split ( ":" );
      string tabla = info_db[1];

      return tabla;
    }

    private string get_campos_db_combobox () {
      var info_db = this.get_campo ().down ().split ( ":" );
      string campos = "";

      for ( int i = 2; i < info_db.length - 1; i++ ) {
        campos += info_db[i] +  ",";
      }
      campos += "id";

      return campos;
    }

    private string get_campos_db_datos () {
      var info_db = this.get_campo ().down ().split ( "=" );

      string campos = "";

      for ( int i = 1; i < info_db.length; i += 2 ) {
        campos += info_db[i];
        if ( i < info_db.length - 2 ) {
          campos +=  ",";
        }
      }
      return campos;
    }

    private void cargar_liststore () {
      Gtk.TreeIter iter;

      var entidades = ApplicationWindow.db.select ( this.get_tabla (), this.get_campos_db_combobox () );

      for ( int i = 0; i < entidades.length; i++ ) {
        var entidad = entidades.index ( i ).split ( "|" );
        string entidad_nombre = "";

        for ( int j = 0; j < entidad.length - 1; j++ ) {
          entidad_nombre += entidad [j] + " ";
        }

        entidad_nombre = entidad_nombre.strip ();
        var entidad_id = entidad[entidad.length -1];

        this.liststore_datos.append ( out iter );
        this.liststore_datos.set ( iter, 0, entidad_nombre,
                                   1, entidad_id );
      }

      if ( entidades.length != 0 ) {
        this.combobox_datos.set_active ( 0 );
      }
    }
  }
}
