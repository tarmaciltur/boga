/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_ingreso_texto_largo.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_ingreso_texto_largo.ui" )]
  public class IngresoTextoLargo : Ingreso {
    [GtkChild]
    private Gtk.TextView textview_datos;

    public IngresoTextoLargo ( string campo ) {
      base ( campo, "[", "]" );
    }

    public override string get_dato () {
      return this.textview_datos.buffer.text;
    }

    [GtkCallback]
    private void cambio () {
      this.cambio_texto ();
    }
  }
}
