/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_administrar_plantillas.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_administrar_plantillas.ui" )]
  public class AdministrarPlantillas : Gtk.Paned {
    [GtkChild]
    private Gtk.ListBox listbox_plantillas;
    [GtkChild]
    private Gtk.Stack stack_plantillas;
    [GtkChild]
    private Boga.Visor visor;
    [GtkChild]
    private Boga.Creador creador;
    [GtkChild]
    private Gtk.Label label_eliminar_plantilla;
    private string id_a_editar;
    private string id_a_eliminar;
    public signal void sin_plantillas ();

    construct {
      this.id_a_editar = "";
      this.id_a_eliminar = "";
    }

    public bool cargar_plantillas () {
      bool retorno = false;
      this.limpiar_plantillas ();
      var ids = ApplicationWindow.db.select ( "plantillas", "id", "" );

      for ( int i = 0; i < ids.length; i++ ) {
        var row = new Boga.AdministrarEntidadesRow ( "plantillas", ids.index ( i ), {"nombre"} );
        this.listbox_plantillas.insert ( row, -1 );
      }

      if ( ids.length > 0 ) {
        retorno = true;
        var row = this.listbox_plantillas.get_row_at_index ( 0 );
        this.listbox_plantillas.select_row ( row );
        this.cargar_ver_plantilla ( ids.index ( 0 ) );
      } else {
        debug ( "no hay plantillas en la DB" );
        this.sin_plantillas ();
      }

      return retorno;
    }

    private void limpiar_plantillas () {
      var row = this.listbox_plantillas.get_row_at_index ( 0 );

      while ( row != null ) {
        row.destroy ();
        row = this.listbox_plantillas.get_row_at_index ( 0 );
      }
    }

    public void ver_plantillas () {
      this.listbox_plantillas.set_visible ( true );
      this.stack_plantillas.set_visible_child_name ( "page_ver" );
    }

    private void cargar_ver_plantilla ( string id ) {
      var definicion = ApplicationWindow.db.select ( "plantillas", "definicion", "WHERE id='" + id + "'" ).index ( 0 );

      var documento = new Boga.Documento ( definicion );

      this.visor.set_documento ( documento );

      this.visor.mostrar_paginas ( new Array<Reemplazo> () );
    }

    [GtkCallback]
    private void listbox_plantillas_row_activated () {
      var row = this.listbox_plantillas.get_selected_row ().get_child () as Boga.AdministrarEntidadesRow;

      this.cargar_ver_plantilla ( row.get_id () );
    }

    public void agregar_plantilla () {
      this.id_a_editar = "";
      this.id_a_eliminar = "";

      this.creador.inicializar ();

      this.listbox_plantillas.set_visible ( false );
      this.stack_plantillas.set_visible_child_name ( "page_editar" );
    }

    public void editar_plantilla () {
      this.id_a_eliminar = "";

      var row = this.listbox_plantillas.get_selected_row ().get_child () as Boga.AdministrarEntidadesRow;

      this.id_a_editar = row.get_id ();

      var plantilla = ApplicationWindow.db.select ( "plantillas", "definicion", "WHERE id='" + this.id_a_editar + "'" ).index ( 0 );
      this.creador.set_plantilla ( plantilla );

      this.listbox_plantillas.set_visible ( false );
      this.stack_plantillas.set_visible_child_name ( "page_editar" );
    }

    public void eliminar_plantilla () {
      this.id_a_editar = "";

      var row = this.listbox_plantillas.get_selected_row ().get_child () as Boga.AdministrarEntidadesRow;

      this.id_a_eliminar = row.get_id ();
      this.label_eliminar_plantilla.set_label ( row.get_label () );

      this.listbox_plantillas.set_visible ( false );
      this.stack_plantillas.set_visible_child_name ( "page_eliminar" );
    }

    public bool agregar_plantilla_aceptar () {
      bool retorno = false;

      if ( this.id_a_eliminar != "" ) {
        retorno = ApplicationWindow.db.del ( "plantillas", "WHERE id='" + this.id_a_eliminar + "'" );
      } else {
        var plantilla = this.creador.get_plantilla ();

        var documento = new Boga.Documento ( plantilla );

        var nombre = documento.get_nombre ();

        if ( this.id_a_editar != "" ) {
          var datos = new Array<string> ();

          datos.append_val ( "nombre = '" + nombre + "'" );
          datos.append_val ( "definicion = '" + plantilla + "'" );

          retorno =  ApplicationWindow.db.update ( "plantillas",
                                                   datos,
                                                   "id = '" + this.id_a_editar + "'" );
        } else {
          retorno = ApplicationWindow.db.insert ( "plantillas", "nombre,definicion", "'" + nombre + "','" + plantilla + "'" );
        }
      }

      return retorno;
    }
  }
}
