/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_hoja.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  public enum Hoja {
    A4,
    CARTA,
    OFICIO;

    public int get_ancho_en_mm () {
      int retorno = 0;
      switch ( this ) {
        case A4:
          retorno = 210;
          break;
        case CARTA:
          retorno = 216;
          break;
        case OFICIO:
          retorno = 216;
          break;
      }

      return retorno;
    }

    public int get_alto_en_mm () {
      int retorno = 0;
      switch ( this ) {
        case A4:
          retorno = 297;
          break;
        case CARTA:
          retorno = 279;
          break;
        case OFICIO:
          retorno = 356;
          break;
      }

      return retorno;
    }
  }
}
