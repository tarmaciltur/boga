/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_creador.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_creador.ui" )]
  public class Creador : Gtk.Paned {
    [GtkChild]
    private Boga.CreadorDocumento creador_documento;
    [GtkChild]
    private Boga.Visor visor;

    construct {
      this.creador_documento.cambio.connect ( this.actualizar_visor );
      this.actualizar_visor ();
    }

    [GtkCallback]
    public void actualizar_visor () {
      var raiz = this.creador_documento.get_nodo ();
      var documento = new Boga.Documento ( raiz.to_string () );

      this.visor.set_documento ( documento );

      this.visor.mostrar_paginas ( new Array<Reemplazo> () );
    }

    public string get_plantilla () {
      var raiz = this.creador_documento.get_nodo ();
      return raiz.to_string ();
    }

    public void set_plantilla ( string plantilla ) {
      this.inicializar ();
      this.creador_documento.set_documento ( new Boga.Documento ( plantilla ) );
    }

    public void inicializar () {
      this.creador_documento.inicializar ();
    }
  }
}
