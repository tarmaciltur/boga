/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_encabezado.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  public class Encabezado : Object {
    private int alto;
    private Array<Seccion> secciones;
    private Array<Imagen> imagenes;

    public Encabezado ( Boga.Nodo raiz, int ancho_seccion ) {
      this.secciones = new Array<Seccion> ();
      this.imagenes = new Array<Imagen> ();

      this.alto = int.parse ( raiz.primer_hijo_nombre ( "alto" ).get_contenido () );

      this.cargar_secciones ( raiz, ancho_seccion );
      this.cargar_imagenes ( raiz );
    }

    private void cargar_secciones ( Boga.Nodo raiz, int ancho_seccion ) {
      var secciones = raiz.listar_hijos_nombre ( "seccion" );

      for ( int i = 0; i < secciones.length; i++ ) {
        Boga.Nodo nodo_seccion = secciones.index ( i );

        string texto = nodo_seccion.primer_hijo_nombre ( "texto" ).get_contenido ();
        string fuente = nodo_seccion.primer_hijo_nombre ( "fuente" ).get_contenido ();
        string peso = nodo_seccion.primer_hijo_nombre ( "peso" ).get_contenido ();
        string sangria = nodo_seccion.primer_hijo_nombre ( "sangria" ).get_contenido ();
        string interlineado = nodo_seccion.primer_hijo_nombre ( "interlineado" ).get_contenido ();
        string justificado = nodo_seccion.primer_hijo_nombre ( "justificado" ).get_contenido ();
        string alineacion = nodo_seccion.primer_hijo_nombre ( "alineacion" ).get_contenido ();
        string desplazamiento_vertical = nodo_seccion.primer_hijo_nombre ( "desplazamiento-vertical" ).get_contenido ();
        string desplazamiento_horizontal = nodo_seccion.primer_hijo_nombre ( "desplazamiento-horizontal" ).get_contenido ();

        int ancho_secciones = ancho_seccion;

        var seccion = new Boga.Seccion ( texto,
                                         fuente,
                                         peso,
                                         sangria,
                                         interlineado,
                                         justificado,
                                         alineacion,
                                         desplazamiento_vertical,
                                         desplazamiento_horizontal,
                                         ancho_secciones );

        this.secciones.append_val ( seccion );
      }
    }

    private void cargar_imagenes ( Boga.Nodo raiz ) {
      var imagenes = raiz.listar_hijos_nombre ( "imagen" );

      for ( int i = 0; i < imagenes.length; i++ ) {
        Boga.Nodo nodo_seccion = imagenes.index ( i );

        string archivo = nodo_seccion.primer_hijo_nombre ( "archivo" ).get_contenido ();
        string x = nodo_seccion.primer_hijo_nombre ( "x" ).get_contenido ();
        string y = nodo_seccion.primer_hijo_nombre ( "y" ).get_contenido ();
        string ancho = nodo_seccion.primer_hijo_nombre ( "ancho" ).get_contenido ();
        string alto = nodo_seccion.primer_hijo_nombre ( "alto" ).get_contenido ();
        string proporcion = nodo_seccion.primer_hijo_nombre ( "proporcion" ).get_contenido ();


        var imagen = new Boga.Imagen ( archivo,
                                       x,
                                       y,
                                       ancho,
                                       alto,
                                       proporcion );

        this.imagenes.append_val ( imagen );
      }
    }

    public Array<Pango.Layout> get_layouts ( Array<Reemplazo> reemplazos, int modificador_resolucion = 1 ) {
      Array<Pango.Layout> layouts = new Array<Pango.Layout> ();

      for ( int i = 0; i < this.secciones.length; i++ ) {
        layouts.append_val ( this.secciones.index ( i ).get_layout ( reemplazos, modificador_resolucion ) );
      }

      return layouts;
    }

    public Array<Imagen> get_imagenes () {
      return this.imagenes;
    }

    public Array<Seccion> get_secciones () {
      return this.secciones;
    }

    public int get_alto_en_mm () {
      return this.alto;
    }

    public int get_alto ( int modificador_resolucion = 1 ) {
      return Utiles.mm_to_px ( this.alto, ApplicationWindow.dpi ) * modificador_resolucion;
    }

  }
}
