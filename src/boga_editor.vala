/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_editor.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_editor.ui" )]
  public class Editor : Gtk.Paned {
    private Boga.Documento documento;
    [GtkChild]
    private Boga.Visor visor;
    [GtkChild]
    private Boga.Ingresos grid_ingresos;
    private Array<Ingreso> ingresos;

    public Editor () {
      this.ingresos = new Array<Ingreso> ();
    }

    public void cargar_plantilla ( string plantilla ) {
      this.documento = new Boga.Documento ( plantilla );

      this.visor.set_documento ( this.documento );

      this.cargar_ingresos ();
      this.cargar_paginas ();
    }

    private void cargar_ingresos () {
      this.grid_ingresos.limpiar_ingresos ();
      var entradas = this.documento.get_entradas ();

      for ( int i = 0; i < entradas.length; i++ ) {
        var entrada = entradas.index ( i );

        switch ( entrada.get_tipo () ) {
          case Boga.Entrada.Tipo.TEXTO_CORTO:
            this.cargar_ingreso ( new Boga.IngresoTextoCorto ( entrada.get_campo () ), i );
            break;
          case Boga.Entrada.Tipo.TEXTO_LARGO:
            this.cargar_ingreso ( new Boga.IngresoTextoLargo ( entrada.get_campo () ), i );
            break;
          case Boga.Entrada.Tipo.ENTIDAD:
            this.cargar_ingreso ( new Boga.IngresoEntidad ( entrada.get_campo () ), i );
            break;
        }
      }

      this.grid_ingresos.alinear_columnas ();
    }

    private void cargar_ingreso ( Boga.Ingreso ingreso, int pos ) {
      ingreso.cambio_texto.connect ( this.cargar_paginas );
      this.grid_ingresos.agregar_ingreso ( ingreso, pos );
    }

    public Array<Reemplazo> cargar_reemplazos () {
      var reemplazos = new Array<Reemplazo> ();
      var ingresos = this.grid_ingresos.get_ingresos ();

      for ( int i= 0; i < ingresos.length; i++ ) {
        var ingreso = ingresos.index ( i );

        if ( ingreso.get_dato () != "" ) {
          var reemplazo = new Reemplazo ( ingreso.get_campo (), ingreso.get_dato () );

          reemplazos.append_val ( reemplazo );
        }
      }

      return reemplazos;
    }

    private void cargar_paginas () {
      var reemplazos = this.cargar_reemplazos ();

      this.visor.mostrar_paginas ( reemplazos );
    }

    public void imprimir ( string archivo ) {
      var reemplazos = this.cargar_reemplazos ();

      this.visor.guardar_pdf ( archivo, reemplazos );
    }
  }
}
