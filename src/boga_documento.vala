/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_documento.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  public class Documento : Object {
    private Boga.Nodo nodo_raiz;
    private Array<Seccion> secciones;
    private Array<Entrada> entradas;
    private Array<Recuadro> recuadros;
    private string nombre;
    private Boga.Hoja hoja;
    private int margen_superior;
    private int margen_inferior;
    private int margen_izquierdo;
    private int margen_derecho;
    private bool margenes_refleados;
    private int ancho;
    private Boga.Encabezado encabezado;

    public Documento ( string plantilla ) {
      this.nodo_raiz = null;
      this.secciones = new Array<Seccion> ();
      this.entradas = new Array<Entrada> ();
      this.recuadros = new Array<Recuadro> ();
      this.encabezado = null;

      if ( plantilla != "" ) {
        this.nodo_raiz = new Nodo.desde_string ( plantilla, 0 );
      } else {
        this.nodo_raiz = new Nodo ( "documento", 0 );
        var nodo_hoja = new Nodo ( "hoja", 1 );
        nodo_hoja.set_contenido ( "A4" );
        this.nodo_raiz.agregar_hijo ( nodo_hoja );
      }

      this.cargar_documento ();
      this.cargar_encabezado ();
      this.cargar_entradas ();
      this.cargar_secciones ();
      this.cargar_recuadros ();
    }

    private void cargar_documento () {
      this.nombre = this.nodo_raiz.primer_hijo_nombre ( "nombre" ).get_contenido ();

      string hoja = this.nodo_raiz.primer_hijo_nombre ( "hoja" ).get_contenido ();
      switch ( hoja ) {
        case "A4":
          this.hoja = Boga.Hoja.A4;
          break;
        case "Carta":
          this.hoja = Boga.Hoja.CARTA;
          break;
        case "Oficio":
          this.hoja = Boga.Hoja.OFICIO;
          break;
      }

      this.ancho = Utiles.mm_to_px ( this.hoja.get_ancho_en_mm (), ApplicationWindow.dpi );

      this.margen_superior = int.parse ( this.nodo_raiz.primer_hijo_nombre ( "margen-superior" ).get_contenido () );

      this.margen_inferior = int.parse ( this.nodo_raiz.primer_hijo_nombre ( "margen-inferior" ).get_contenido () );

      this.margen_izquierdo = int.parse ( this.nodo_raiz.primer_hijo_nombre ( "margen-izquierdo" ).get_contenido () );

      this.margen_derecho = int.parse ( this.nodo_raiz.primer_hijo_nombre ( "margen-derecho" ).get_contenido () );

      bool margenes_refleados = bool.parse ( this.nodo_raiz.primer_hijo_nombre ( "margenes-reflejados" ).get_contenido () );
      this.margenes_refleados = margenes_refleados;
    }

    private void cargar_entradas () {
      var secciones = this.nodo_raiz.listar_hijos_nombre ( "seccion" );

      for ( int i = 0; i < secciones.length; i++ ) {
        Boga.Nodo nodo_seccion = secciones.index ( i );

        string texto = nodo_seccion.primer_hijo_nombre ( "texto" ).get_contenido ();

        for ( int j = 0; j < texto.length; j++ ) {
          switch ( texto[j] ) {
            case '{':
              int cierre_llave = texto.index_of ( "}", j + 1 );
              if ( cierre_llave != -1 ) {
                var campo = texto.substring ( j + 1, texto.index_of ( "}", j + 1 ) - 1 - j );
                var entrada = new Boga.Entrada.texto_corto ( campo );

                this.entradas.append_val ( entrada );
                j = texto.index_of ( "}", j );
              }
              break;
            case '[':
              int cierre_llave = texto.index_of ( "]", j + 1 );
              if ( cierre_llave != -1 ) {
                var campo = texto.substring ( j + 1, texto.index_of ( "]", j + 1 ) - 1 - j );
                var entrada = new Boga.Entrada.texto_largo ( campo );

                this.entradas.append_val ( entrada );
                j = texto.index_of ( "]", j );
              }
              break;
            case '|':
              int cierre_llave = texto.index_of ( "|", j + 1 );
              if ( cierre_llave != -1 ) {
                var campo = texto.substring ( j + 1, texto.index_of ( "|", j + 1 ) - 1 - j );
                var entrada = new Boga.Entrada.entidad ( campo );

                this.entradas.append_val ( entrada );
                j = texto.index_of ( "|", j + 1 );
              }
              break;
          }
        }
      }
    }

    private void cargar_secciones () {
      var secciones = this.nodo_raiz.listar_hijos_nombre ( "seccion" );

      for ( int i = 0; i < secciones.length; i++ ) {
        Boga.Nodo nodo_seccion = secciones.index ( i );

        string texto = nodo_seccion.primer_hijo_nombre ( "texto" ).get_contenido ();
        string fuente = nodo_seccion.primer_hijo_nombre ( "fuente" ).get_contenido ();
        string peso = nodo_seccion.primer_hijo_nombre ( "peso" ).get_contenido ();
        string sangria = nodo_seccion.primer_hijo_nombre ( "sangria" ).get_contenido ();
        string interlineado = nodo_seccion.primer_hijo_nombre ( "interlineado" ).get_contenido ();
        string justificado = nodo_seccion.primer_hijo_nombre ( "justificado" ).get_contenido ();
        string alineacion = nodo_seccion.primer_hijo_nombre ( "alineacion" ).get_contenido ();
        string desplazamiento_vertical = nodo_seccion.primer_hijo_nombre ( "desplazamiento-vertical" ).get_contenido ();
        string desplazamiento_horizontal = nodo_seccion.primer_hijo_nombre ( "desplazamiento-horizontal" ).get_contenido ();

        int ancho_secciones = this.ancho - ( this.get_margen_izquierdo () + this.get_margen_derecho () );

        var seccion = new Boga.Seccion ( texto,
                                         fuente,
                                         peso,
                                         sangria,
                                         interlineado,
                                         justificado,
                                         alineacion,
                                         desplazamiento_vertical,
                                         desplazamiento_horizontal,
                                         ancho_secciones );

        this.secciones.append_val ( seccion );
      }
    }

    public void cargar_encabezado () {
      var nodo_encabezado = this.nodo_raiz.primer_hijo_nombre ( "encabezado" );
      if ( nodo_encabezado.primer_hijo_nombre ( "alto" ).get_contenido () != "" ) {
        this.encabezado = new Boga.Encabezado ( nodo_encabezado, this.ancho - ( this.get_margen_izquierdo () + this.get_margen_derecho () ) );
      }
    }

    public void cargar_recuadros () {
      var recuadros = this.nodo_raiz.listar_hijos_nombre ( "recuadro" );

      for ( int i = 0; i < recuadros.length; i++ ) {
        Boga.Nodo nodo_recuadro = recuadros.index ( i );

        string pagina = nodo_recuadro.primer_hijo_nombre ( "pagina" ).get_contenido ();
        string x = nodo_recuadro.primer_hijo_nombre ( "x" ).get_contenido ();
        string y = nodo_recuadro.primer_hijo_nombre ( "y" ).get_contenido ();
        string ancho = nodo_recuadro.primer_hijo_nombre ( "ancho" ).get_contenido ();
        string alto = nodo_recuadro.primer_hijo_nombre ( "alto" ).get_contenido ();
        string proporcion = nodo_recuadro.primer_hijo_nombre ( "proporcion" ).get_contenido ();

        var recuadro = new Boga.Recuadro ( pagina,
                                           x,
                                           y,
                                           ancho,
                                           alto,
                                           proporcion );

        this.recuadros.append_val ( recuadro );
      }
    }

    public Array<Entrada> get_entradas () {
      return this.entradas;
    }

    public Array<Seccion> get_secciones () {
      return this.secciones;
    }

    public Array<Recuadro> get_recuadros () {
      return this.recuadros;
    }

    public Array<Pango.Layout> get_layouts ( Array<Reemplazo> reemplazos, int modificador_resolucion = 1 ) {
      Array<Pango.Layout> layouts = new Array<Pango.Layout> ();

      for ( int i = 0; i < this.secciones.length; i++ ) {
        layouts.append_val ( this.secciones.index ( i ).get_layout ( reemplazos, modificador_resolucion ) );
      }

      return layouts;
    }

    public string get_nombre () {
      return this.nombre;
    }

    public int get_ancho ( int modificador_resolucion = 1 ) {
      return Utiles.mm_to_px ( this.hoja.get_ancho_en_mm (), ApplicationWindow.dpi ) * modificador_resolucion;
    }

    public int get_alto ( int modificador_resolucion = 1 ) {
      return Utiles.mm_to_px ( this.hoja.get_alto_en_mm (), ApplicationWindow.dpi ) * modificador_resolucion;
    }

    public int get_margen_superior ( int modificador_resolucion = 1 ) {
      return Utiles.mm_to_px ( this.margen_superior, ApplicationWindow.dpi ) * modificador_resolucion;
    }

    public int get_margen_inferior ( int modificador_resolucion = 1 ) {
      return Utiles.mm_to_px ( this.margen_inferior, ApplicationWindow.dpi ) * modificador_resolucion;
    }

    public int get_margen_izquierdo ( int modificador_resolucion = 1 ) {
      return Utiles.mm_to_px ( this.margen_izquierdo, ApplicationWindow.dpi ) * modificador_resolucion;
    }

    public int get_margen_derecho ( int modificador_resolucion = 1 ) {
      return Utiles.mm_to_px ( this.margen_derecho, ApplicationWindow.dpi ) * modificador_resolucion;
    }

    public int get_margen_superior_en_mm () {
      return this.margen_superior;
    }

    public int get_margen_inferior_en_mm () {
      return this.margen_inferior;
    }

    public int get_margen_izquierdo_en_mm () {
      return this.margen_izquierdo;
    }

    public int get_margen_derecho_en_mm () {
      return this.margen_derecho;
    }

    public bool get_margenes_reflejados () {
      return this.margenes_refleados;
    }

    public Boga.Encabezado ? get_encabezado () {
      return this.encabezado;
    }

    public Boga.Hoja get_hoja () {
      return this.hoja;
    }
  }
}
