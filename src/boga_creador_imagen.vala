/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_creador_imagen.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_creador_imagen.ui" )]
  class CreadorImagen : Gtk.Grid {
    [GtkChild]
    private Gtk.Image image_button_revealer;
    [GtkChild]
    private Gtk.Revealer revealer;
    [GtkChild]
    private Gtk.Revealer revealer_button_opciones;
    [GtkChild]
    private Gtk.SpinButton spinbutton_x;
    [GtkChild]
    private Gtk.SpinButton spinbutton_y;
    [GtkChild]
    private Gtk.SpinButton spinbutton_ancho;
    [GtkChild]
    private Gtk.SpinButton spinbutton_alto;
    [GtkChild]
    private Gtk.Label label_alto;
    [GtkChild]
    private Gtk.Switch switch_proporcion;
    [GtkChild]
    private Gtk.FileChooserButton file_chooser_button_image;
    private int nivel;

    public signal void cambio ();

    public CreadorImagen ( int nivel ) {
      this.nivel = nivel;
    }

    [GtkCallback]
    public void button_revealer_clicked () {
      if ( this.revealer.get_reveal_child () == false ) {
        this.image_button_revealer.set_from_icon_name ( "go-up-symbolic", Gtk.IconSize.BUTTON );
        this.revealer.set_reveal_child ( true );
        this.revealer_button_opciones.set_reveal_child ( true );
      } else {
        this.image_button_revealer.set_from_icon_name ( "go-down-symbolic", Gtk.IconSize.BUTTON );
        this.revealer.set_reveal_child ( false );
        this.revealer_button_opciones.set_reveal_child ( false );
      }
    }

    public Boga.Nodo get_nodo () {
      var nodo_imagen = new Boga.Nodo ( "imagen", this.nivel );

      var nodo_archivo = new Boga.Nodo ( "archivo", this.nivel + 1 );
      if ( this.file_chooser_button_image.get_filename () != null ) {
        nodo_archivo.set_contenido ( this.file_chooser_button_image.get_filename () );
      } else {
        nodo_archivo.set_contenido ( "" );
      }
      nodo_imagen.agregar_hijo ( nodo_archivo );

      var nodo_x = new Boga.Nodo ( "x", this.nivel + 1 );
      if ( this.spinbutton_x.text != "" ) {
        nodo_x.set_contenido ( this.spinbutton_x.text );
      } else {
        nodo_x.set_contenido ( "0" );
      }
      nodo_imagen.agregar_hijo ( nodo_x );

      var nodo_y = new Boga.Nodo ( "y", this.nivel + 1 );
      if ( this.spinbutton_y.text != "" ) {
        nodo_y.set_contenido ( this.spinbutton_y.text );
      } else {
        nodo_y.set_contenido ( "0" );
      }
      nodo_imagen.agregar_hijo ( nodo_y );

      var nodo_ancho = new Boga.Nodo ( "ancho", this.nivel + 1 );
      if ( this.spinbutton_ancho.text != "" ) {
        nodo_ancho.set_contenido ( this.spinbutton_ancho.text );
      } else {
        nodo_ancho.set_contenido ( "0" );
      }
      nodo_imagen.agregar_hijo ( nodo_ancho );

      var nodo_alto = new Boga.Nodo ( "alto", this.nivel + 1 );
      if ( this.spinbutton_alto.text != "" && this.switch_proporcion.get_active () == false ) {
        nodo_alto.set_contenido ( this.spinbutton_alto.text );
      } else {
        nodo_alto.set_contenido ( "-1" );
      }
      nodo_imagen.agregar_hijo ( nodo_alto );

      var nodo_proporcion = new Boga.Nodo ( "proporcion", this.nivel + 1 );
      nodo_proporcion.set_contenido ( this.switch_proporcion.get_active ().to_string () );
      nodo_imagen.agregar_hijo ( nodo_proporcion );

      return nodo_imagen;
    }

    public void set_imagen ( Boga.Imagen imagen ) {
      if ( Doctrina.Infraestructura.existe_path ( imagen.get_archivo () ) ) {
        this.file_chooser_button_image.set_filename ( imagen.get_archivo () );
      }

      this.spinbutton_x.set_value ( imagen.get_x_en_mm () );
      this.spinbutton_y.set_value ( imagen.get_y_en_mm () );
      this.spinbutton_ancho.set_value ( imagen.get_ancho_en_mm () );
      this.spinbutton_alto.set_value ( imagen.get_alto_en_mm () );

      this.switch_proporcion.set_active ( imagen.get_proporcion () );
    }

    [GtkCallback]
    private void actualizar () {
      this.cambio ();
    }

    [GtkCallback]
    private void switch_proporcion_changed () {
      if ( this.switch_proporcion.get_active () == true ) {
        this.spinbutton_alto.set_sensitive ( false );
        this.label_alto.set_sensitive ( false );
      } else {
        this.spinbutton_alto.set_sensitive ( true );
        this.label_alto.set_sensitive ( true );
      }

      this.actualizar ();
    }
  }
}
