/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_seccion.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  public class Seccion : Object {
    private string texto;
    private string fuente;
    private string peso;
    private int sangria;
    private int interlineado;
    private bool justificado;
    private string alineacion;
    private int desplazamiento_vertical;
    private int desplazamiento_horizontal;
    private int ancho;
    private Pango.Layout layout;

    public Seccion ( string texto,
                     string fuente,
                     string peso,
                     string sangria,
                     string interlineado,
                     string justificado,
                     string alineacion,
                     string desplazamiento_vertical,
                     string desplazamiento_horizontal,
                     int ancho ) {
      this.texto = texto;
      this.fuente = fuente;
      this.peso = peso;
      this.sangria = int.parse ( sangria );
      this.interlineado = int.parse ( interlineado );
      this.justificado = bool.parse ( justificado );
      this.alineacion = alineacion;
      this.desplazamiento_vertical = int.parse ( desplazamiento_vertical );
      this.desplazamiento_horizontal = int.parse ( desplazamiento_horizontal );
      this.ancho = ancho;

    }

    public void crear_layout ( int modificador_resolucion = 1 ) {
      Pango.Context pango_context = Gdk.pango_context_get ();

      var fuente = Pango.FontDescription.from_string ( this.fuente );
      if ( this.peso == "bold" ) {
        fuente.set_weight ( Pango.Weight.BOLD );
      }

      pango_context.set_font_description ( fuente );

      Pango.cairo_context_set_resolution ( pango_context, ApplicationWindow.dpi * 1.0 * modificador_resolucion );

      var pango_layout = new Pango.Layout ( pango_context );

      var sangria = Utiles.mm_to_px ( this.sangria, ApplicationWindow.dpi );
      pango_layout.set_indent ( sangria * Pango.SCALE * modificador_resolucion );

      var interlineado = Utiles.mm_to_px ( this.interlineado, ApplicationWindow.dpi );
      pango_layout.set_spacing ( interlineado * Pango.SCALE * modificador_resolucion );

      var justificado = this.justificado;
      pango_layout.set_justify ( justificado );
      pango_layout.set_width ( ( this.ancho - this.desplazamiento_horizontal )  * Pango.SCALE * modificador_resolucion );

      if ( this.alineacion == "center" ) {
        pango_layout.set_alignment ( Pango.Alignment.CENTER );
      } else {
        if ( this.alineacion == "right" ) {
          pango_layout.set_alignment ( Pango.Alignment.RIGHT );
        } else {
          pango_layout.set_alignment ( Pango.Alignment.LEFT );
        }
      }

      this.layout = pango_layout;
    }

    public Pango.Layout get_layout ( Array<Reemplazo> reemplazos, int modificador_resolucion = 1 ) {
      string texto = this.texto;

      for ( int i = 0; i < reemplazos.length; i++ ) {
        var reemplazo = reemplazos.index ( i );

        var texto_temporal = texto.replace ( reemplazo.get_campo (), reemplazo.get_valor () );

        texto = texto_temporal;
      }

      this.crear_layout ( modificador_resolucion );
      this.layout.set_text ( texto, texto.length );

      return this.layout;
    }

    public int get_desplazamiento_vertical () {
      return this.desplazamiento_vertical;
    }

    public int get_desplazamiento_horizontal () {
      return this.desplazamiento_horizontal;
    }

    public string get_texto () {
      return this.texto;
    }

    public string get_alineacion () {
      return this.alineacion;
    }

    public bool get_justificado () {
      return this.justificado;
    }

    public int get_sangria () {
      return this.sangria;
    }

    public int get_tamanio_fuente () {
      return int.parse ( this.fuente.split ( " " )[1] );
    }

    public string get_fuente () {
      return this.fuente.split ( " " )[0];
    }

    public int get_interlineado () {
      return this.interlineado;
    }

    public string get_peso () {
      return this.peso;
    }
  }
}
