/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_imagen.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  public class Imagen : Object {
    private Gdk.Pixbuf pixbuf;
    private string archivo;
    private int x;
    private int y;
    private int ancho;
    private int alto;
    private bool proporcion;
    private double escala;

    public Imagen ( string archivo,
                    string x,
                    string y,
                    string ancho,
                    string alto,
                    string proporcion ) {
      this.archivo = archivo;
      this.x = int.parse ( x );
      this.y = int.parse ( y );
      this.ancho = int.parse ( ancho );
      this.alto = int.parse ( alto );
      this.proporcion = bool.parse ( proporcion );
      this.pixbuf = null;

      var data = new Bytes ( { 0, 0, 0, 0 } );
      this.pixbuf = new Gdk.Pixbuf.from_bytes ( data, Gdk.Colorspace.RGB, true, 8, 1, 1, 1 );

      if ( this.archivo != "" && Doctrina.Infraestructura.existe_path ( this.archivo ) ) {
        try {
          this.pixbuf = new Gdk.Pixbuf.from_file ( this.archivo );
        } catch ( Error e ) {
          debug ( e.message );
        }
      }

      var ancho_original = this.pixbuf.get_width ();
      this.escala = (this.get_ancho () * 1.0) / (ancho_original * 1.0);
    }

    public int get_x () {
      return Boga.Utiles.mm_to_px ( this.x, ApplicationWindow.dpi );
    }

    public int get_y () {
      return Boga.Utiles.mm_to_px ( this.y, ApplicationWindow.dpi );
    }

    public int get_ancho () {
      return Boga.Utiles.mm_to_px ( this.ancho, ApplicationWindow.dpi );;
    }

    public int get_alto () {
      return Boga.Utiles.mm_to_px ( this.alto, ApplicationWindow.dpi );;
    }

    public string get_archivo () {
      return this.archivo;
    }

    public int get_x_en_mm () {
      return this.x;
    }

    public int get_y_en_mm () {
      return this.y;
    }

    public int get_ancho_en_mm () {
      return this.ancho;
    }

    public int get_alto_en_mm () {
      return this.alto;
    }

    public bool get_proporcion () {
      return this.proporcion;
    }

    public double get_escala () {
      return this.escala;
    }

    public Gdk.Pixbuf ? get_pixbuf ( int modificador_resolucion = 1 ) {
      double alto = 1;
      if ( this.proporcion == true ) {
        alto = this.pixbuf.get_height () * this.escala;
      } else {
        alto = this.get_alto ();
      }
      return this.pixbuf.scale_simple ( this.get_ancho () * modificador_resolucion,
                                        (int)alto * modificador_resolucion,
                                        Gdk.InterpType.HYPER );
    }
  }
}
