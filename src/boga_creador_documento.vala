/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_creador_documento.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_creador_documento.ui" )]
  class CreadorDocumento : Gtk.Grid {
    [GtkChild]
    private Gtk.Image image_button_revealer;
    [GtkChild]
    private Gtk.Button button_agregar_encabezado;
    [GtkChild]
    private Gtk.Revealer revealer;
    [GtkChild]
    private Gtk.Revealer revealer_formato;
    [GtkChild]
    private Gtk.Entry entry_nombre;
    [GtkChild]
    private Gtk.ComboBox combobox_tamanio_hoja;
    [GtkChild]
    private Gtk.ListStore liststore_tamanio_hoja;
    [GtkChild]
    private Gtk.SpinButton spinbutton_margen_superior;
    [GtkChild]
    private Gtk.SpinButton spinbutton_margen_inferior;
    [GtkChild]
    private Gtk.SpinButton spinbutton_margen_izquierdo;
    [GtkChild]
    private Gtk.SpinButton spinbutton_margen_derecho;
    [GtkChild]
    private Gtk.Switch switch_margenes_reflejados;
    [GtkChild]
    private Gtk.ListBox listbox_secciones;
    [GtkChild]
    private Gtk.ListBox listbox_recuadros;
    private Boga.CreadorEncabezado encabezado;
    private Boga.Nodo raiz;
    private int nivel;
    private int cantidad_secciones;
    private int cantidad_recuadros;

    public signal void cambio ();

    public CreadorDocumento () {
      this.nivel = 0;
      this.inicializar ();
    }

    public void inicializar () {
      this.encabezado = null;

      var widget = this.get_child_at ( 0, 2 );
      if ( widget != null ) {
        widget.destroy ();
      }

      this.cantidad_secciones = 0;
      this.cantidad_recuadros = 0;

      this.limpiar_listbox ( this.listbox_secciones );
      this.limpiar_listbox ( this.listbox_recuadros );

      this.cambio ();
    }

    private void limpiar_listbox ( Gtk.ListBox listbox ) {
      var row = listbox.get_row_at_index ( 0 );

      while ( row != null ) {
        row.destroy ();
        row = listbox.get_row_at_index ( 0 );
      }
    }

    [GtkCallback]
    public void button_revealer_clicked () {
      if ( this.revealer.get_reveal_child () == false ) {
        this.image_button_revealer.set_from_icon_name ( "go-up-symbolic", Gtk.IconSize.BUTTON );
        this.revealer.set_reveal_child ( true );
        this.revealer_formato.set_reveal_child ( true );
      } else {
        this.image_button_revealer.set_from_icon_name ( "go-down-symbolic", Gtk.IconSize.BUTTON );
        this.revealer.set_reveal_child ( false );
        this.revealer_formato.set_reveal_child ( false );
      }
    }

    public Boga.Nodo get_nodo () {
      this.raiz = new Boga.Nodo ( "documento", this.nivel );

      var nodo_nombre = new Boga.Nodo ( "nombre", this.nivel + 1 );
      if ( this.entry_nombre.text != "" ) {
        nodo_nombre.set_contenido ( this.entry_nombre.text );
      } else {
        nodo_nombre.set_contenido ( "" );
      }
      this.raiz.agregar_hijo ( nodo_nombre );

      var nodo_tamanio = new Boga.Nodo ( "hoja", this.nivel + 1 );

      Gtk.TreeIter iter;
      if ( this.combobox_tamanio_hoja.get_active_iter ( out iter ) ) {
        Value valor;
        this.liststore_tamanio_hoja.get_value ( iter, 0, out valor );
        nodo_tamanio.set_contenido ( (string)valor );
      } else {
        nodo_tamanio.set_contenido ( "A4" );
      }

      this.raiz.agregar_hijo ( nodo_tamanio );

      var nodo_margen_superior = new Boga.Nodo ( "margen-superior", this.nivel + 1 );
      if ( this.spinbutton_margen_superior.text != "" ) {
        nodo_margen_superior.set_contenido ( this.spinbutton_margen_superior.text );
      } else {
        nodo_margen_superior.set_contenido ( "0" );
      }
      this.raiz.agregar_hijo ( nodo_margen_superior );

      var nodo_margen_inferior = new Boga.Nodo ( "margen-inferior", this.nivel + 1 );
      if ( this.spinbutton_margen_inferior.text != "" ) {
        nodo_margen_inferior.set_contenido ( this.spinbutton_margen_inferior.text );
      } else {
        nodo_margen_inferior.set_contenido ( "0" );
      }
      this.raiz.agregar_hijo ( nodo_margen_inferior );

      var nodo_margen_izquierdo = new Boga.Nodo ( "margen-izquierdo", this.nivel + 1 );
      if ( this.spinbutton_margen_izquierdo.text != "" ) {
        nodo_margen_izquierdo.set_contenido ( this.spinbutton_margen_izquierdo.text );
      } else {
        nodo_margen_izquierdo.set_contenido ( "0" );
      }
      this.raiz.agregar_hijo ( nodo_margen_izquierdo );

      var nodo_margen_derecho = new Boga.Nodo ( "margen-derecho", this.nivel + 1 );
      if ( this.spinbutton_margen_derecho.text != "" ) {
        nodo_margen_derecho.set_contenido ( this.spinbutton_margen_derecho.text );
      } else {
        nodo_margen_derecho.set_contenido ( "0" );
      }
      this.raiz.agregar_hijo ( nodo_margen_derecho );

      var nodo_margenes_reflejados = new Boga.Nodo ( "margenes-reflejados", this.nivel + 1 );
      nodo_margenes_reflejados.set_contenido ( this.switch_margenes_reflejados.get_active ().to_string () );
      this.raiz.agregar_hijo ( nodo_margenes_reflejados );

      if ( this.encabezado != null ) {
        this.raiz.agregar_hijo ( this.encabezado.get_nodo () );
      }

      this.agregar_nodo_secciones ();
      this.agregar_nodo_recuadros ();

      return this.raiz;
    }

    private void agregar_nodo_secciones () {
      int i = 0;
      Gtk.ListBoxRow row = this.listbox_secciones.get_row_at_index ( i );

      while ( row != null ) {
        Boga.CreadorSeccion seccion = row.get_child () as Boga.CreadorSeccion;
        this.raiz.agregar_hijo ( seccion.get_nodo () );
        i++;
        row = this.listbox_secciones.get_row_at_index ( i );
      }
    }

    private void agregar_nodo_recuadros () {
      int i = 0;
      Gtk.ListBoxRow row = this.listbox_recuadros.get_row_at_index ( i );

      while ( row != null ) {
        Boga.CreadorRecuadro recuadro = row.get_child () as Boga.CreadorRecuadro;
        this.raiz.agregar_hijo ( recuadro.get_nodo () );
        i++;
        row = this.listbox_recuadros.get_row_at_index ( i );
      }
    }

    [GtkCallback]
    private void actualizar () {
      this.cambio ();
    }

    [GtkCallback]
    private void button_agregar_encabezado_clicked () {
      this.encabezado = new Boga.CreadorEncabezado ( this.nivel + 1 );
      this.encabezado.cambio.connect ( this.actualizar );

      this.attach ( this.encabezado, 0, 2, 3, 1 );
      this.button_agregar_encabezado.set_sensitive ( false );
    }

    [GtkCallback]
    private void button_agregar_seccion_clicked () {
      var seccion = new Boga.CreadorSeccion ( this.nivel + 1 );
      seccion.cambio.connect ( this.actualizar );

      this.listbox_secciones.insert ( seccion, -1 );
      this.sacar_foco_row ( this.listbox_secciones );

      this.cantidad_secciones++;
    }

    [GtkCallback]
    private void button_agregar_recuadro_clicked () {
      var recuadro = new Boga.CreadorRecuadro ( this.nivel + 1 );
      recuadro.cambio.connect ( this.actualizar );

      this.listbox_recuadros.insert ( recuadro, -1 );
      this.sacar_foco_row ( this.listbox_recuadros );

      this.cantidad_recuadros++;
    }

    private void sacar_foco_row ( Gtk.ListBox listbox ) {
      int i = 0;
      Gtk.ListBoxRow row = listbox.get_row_at_index ( i );

      while ( row != null ) {
        row.set_can_focus ( false );
        i++;
        row = listbox.get_row_at_index ( i );
      }
    }

    private void set_combobox_tamanio_hoja_selection ( Boga.Hoja hoja ) {
      Gtk.TreeIter iter;
      bool coincide = false;
      Value valor;

      var model = this.combobox_tamanio_hoja.get_model ();

      if ( model.get_iter_first ( out iter ) ) {
        do {
          model.get_value ( iter, 0, out valor );

          if ( hoja.to_string () == (string)valor ) {
            coincide = true;
            break;
          }
        } while ( model.iter_next ( ref iter ) );

        if ( coincide ) {
          this.combobox_tamanio_hoja.set_active_iter ( iter );
        }

      }
    }

    public void set_documento ( Boga.Documento documento ) {
      this.inicializar ();
      this.entry_nombre.set_text ( documento.get_nombre () );

      this.spinbutton_margen_superior.set_value ( (double)documento.get_margen_superior_en_mm () );
      this.spinbutton_margen_inferior.set_value ( (double)documento.get_margen_inferior_en_mm () );
      this.spinbutton_margen_izquierdo.set_value ( (double)documento.get_margen_izquierdo_en_mm () );
      this.spinbutton_margen_derecho.set_value ( (double)documento.get_margen_derecho_en_mm () );

      this.set_combobox_tamanio_hoja_selection ( documento.get_hoja () );

      this.switch_margenes_reflejados.set_active ( documento.get_margenes_reflejados () );

      if ( documento.get_encabezado () != null ) {
        this.button_agregar_encabezado_clicked ();
        this.encabezado.set_encabezado ( documento.get_encabezado () );
      }

      var secciones = documento.get_secciones ();
      for ( int i = 0; i < secciones.length; i++ ) {
        this.button_agregar_seccion_clicked ();
        var creador_seccion = this.listbox_secciones.get_row_at_index ( this.cantidad_secciones -1 ).get_child () as Boga.CreadorSeccion;

        creador_seccion.set_seccion ( secciones.index ( i ) );
      }

      var recuadros = documento.get_recuadros ();
      for ( int i = 0; i < recuadros.length; i++ ) {
        this.button_agregar_recuadro_clicked ();
        var creador_recuadro = this.listbox_recuadros.get_row_at_index ( this.cantidad_recuadros -1 ).get_child () as Boga.CreadorRecuadro;

        creador_recuadro.set_recuadro ( recuadros.index ( i ) );
      }

      this.cambio ();
    }
  }
}
