/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_visor.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_visor.ui" )]
  public class Visor : Gtk.Grid {
    private int ancho;
    private int alto;
    private int dpi;
    private Boga.Documento documento;

    construct {
      this.dpi = ApplicationWindow.dpi;

      this.set_documento ( new Boga.Documento ( "" ) );

      this.crear_paginas ( new Array<Reemplazo> (), 1 );
    }

    public void set_documento ( Boga.Documento documento ) {
      this.ancho = documento.get_ancho ();
      this.alto =  documento.get_alto ();

      this.documento = documento;

      this.borrar_paginas ();
    }

    public void mostrar_paginas ( Array<Reemplazo> reemplazos ) {
      this.borrar_paginas ();

      var paginas = this.crear_paginas ( reemplazos, 1 );

      for ( int i = 0; i < paginas.length; i++ ) {
        var pagina = paginas.index ( i );

        this.mostrar_pagina ( pagina, i );
      }
    }

    private Array<Gdk.Pixbuf> crear_paginas ( Array<Reemplazo> reemplazos, int modificador_resolucion, string archivo = "" ) {
      int alto_sin_margen_inferior = ( this.alto - documento.get_margen_inferior () ) * modificador_resolucion;
      int alto_linea = 0;
      int margen_izquierdo = 0;
      int margen_superior = this.documento.get_margen_superior () * modificador_resolucion;
      int posicion_inicial = 0;
      int baseline = 0;
      int cantidad_paginas = 0;
      int alto_a_dibujar;
      int dibujado_anterior = 0;
      int ancho = this.ancho * modificador_resolucion;
      int alto = this.alto * modificador_resolucion;
      double res_x = 0;
      double res_y = 0;
      Cairo.Surface pagina_surface;
      Cairo.Surface texto_surface;
      Cairo.Context pagina_context;
      Cairo.Context texto_context;
      Pango.Rectangle logical;
      Pango.LayoutIter iter;
      Array<Gdk.Pixbuf> paginas = new Array<Gdk.Pixbuf> ();

      var layouts = this.documento.get_layouts ( reemplazos, modificador_resolucion );
      var secciones = this.documento.get_secciones ();

      pagina_surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, ancho, alto );
      if ( archivo != "" ) {
        pagina_surface = new Cairo.PdfSurface  ( archivo, this.ancho, this.alto );

        pagina_surface.get_fallback_resolution ( out res_x, out res_y );
        pagina_surface.set_fallback_resolution ( res_x * modificador_resolucion, res_y * modificador_resolucion );
        pagina_surface.set_device_scale ( 1/(double)modificador_resolucion, 1/(double)modificador_resolucion );
      }

      pagina_context = new Cairo.Context ( pagina_surface );

      if ( archivo == "" ) {
        texto_surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, ancho, alto );
        texto_context = new Cairo.Context ( texto_surface );

        this.crear_texto_context ( cantidad_paginas, modificador_resolucion, out texto_context );
      } else {
        texto_context = pagina_context;
      }

      margen_izquierdo = this.calcular_margen_izquierdo ( cantidad_paginas ) * modificador_resolucion;

      var encabezado_context = this.dibujar_encabezado ( reemplazos, margen_izquierdo, modificador_resolucion );
      var recuadros_context = this.dibujar_recuadros ( cantidad_paginas, modificador_resolucion );

      for ( int i = 0; i < layouts.length; i++ ) {
        iter = layouts.index ( i ).get_iter ();
        var desplazado = false;

        do {
          var layout_line = iter.get_line_readonly ();
          iter.get_line_extents ( null, out logical );
          baseline = iter.get_baseline () / Pango.SCALE;
          alto_linea = alto_linea = (logical.height + layouts.index ( i ).get_spacing () ) / Pango.SCALE;

          posicion_inicial = logical.y / Pango.SCALE;

          if ( desplazado == false ) {
            var desplazamiento_vertical = secciones.index ( i ).get_desplazamiento_vertical ();
            dibujado_anterior += alto_linea * desplazamiento_vertical;
            desplazado = true;
          }

          var desplazamiento_horizontal = secciones.index ( i ).get_desplazamiento_horizontal () * modificador_resolucion;

          alto_a_dibujar = dibujado_anterior +
                           margen_superior +
                           baseline -
                           posicion_inicial +
                           alto_linea;

          if ( alto_a_dibujar > alto_sin_margen_inferior ) {
            pagina_context.save ();
            if ( archivo == "" ) {
              pagina_context.set_operator ( Cairo.Operator.OVER );
              pagina_context.set_source_surface ( texto_context.get_target (), 0, 0 );
              pagina_context.paint ();
            }
            pagina_context.set_operator ( Cairo.Operator.OVER );
            pagina_context.set_source_surface ( encabezado_context.get_target (), 0, 0 );
            pagina_context.paint ();
            pagina_context.set_operator ( Cairo.Operator.OVER );
            pagina_context.set_source_surface ( recuadros_context.get_target (), 0, 0 );
            pagina_context.paint ();
            pagina_context.restore ();

            var pixbuf = Gdk.pixbuf_get_from_surface ( pagina_context.get_target (),
                                                       0, 0, ancho, alto );
            paginas.append_val ( pixbuf );

            if ( archivo == "" ) {
              pagina_surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, ancho, alto );
              pagina_context = new Cairo.Context ( pagina_surface );

              this.crear_texto_context ( cantidad_paginas, modificador_resolucion, out pagina_context );
            } else {
              pagina_context.show_page ();
            }

            cantidad_paginas++;
            alto_a_dibujar = 0;
            dibujado_anterior = 0;

            margen_izquierdo = this.calcular_margen_izquierdo ( cantidad_paginas ) * modificador_resolucion;
            encabezado_context = this.dibujar_encabezado ( reemplazos, margen_izquierdo, modificador_resolucion );

            if ( archivo == "" ) {
              texto_surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, ancho, alto );
              texto_context = new Cairo.Context ( texto_surface );
            }

            recuadros_context = this.dibujar_recuadros ( cantidad_paginas, modificador_resolucion );
          }
          texto_context.move_to ( margen_izquierdo + desplazamiento_horizontal + logical.x / Pango.SCALE,
                                  dibujado_anterior + margen_superior + baseline - posicion_inicial );
          Pango.cairo_show_layout_line ( texto_context, layout_line );

          dibujado_anterior += alto_linea;
        } while ( iter.next_line () );
      }

      if ( archivo == "" ) {
        pagina_context.set_operator ( Cairo.Operator.OVER );
        pagina_context.set_source_surface ( texto_context.get_target (), 0, 0 );
        pagina_context.paint ();
      }

      pagina_context.set_operator ( Cairo.Operator.OVER );
      pagina_context.set_source_surface ( encabezado_context.get_target (), 0, 0 );
      pagina_context.paint ();
      pagina_context.set_operator ( Cairo.Operator.OVER );
      pagina_context.set_source_surface ( recuadros_context.get_target (), 0, 0 );
      pagina_context.paint ();


      var pixbuf = Gdk.pixbuf_get_from_surface ( pagina_context.get_target (),
                                                 0, 0, ancho, alto );
      paginas.append_val ( pixbuf );

      if ( archivo != "" ) {
        pagina_context.show_page ();
      }

      return paginas;
    }

    private void mostrar_pagina ( Gdk.Pixbuf pixbuf, int posicion ) {
      var pagina = new Boga.VisorPagina ( pixbuf );
      pagina.set_visible ( true );

      this.attach ( pagina, 0, posicion, 1, 1 );
    }

    private void crear_texto_context ( int cantidad_paginas, int modificador_resolucion, out Cairo.Context context ) {
      int ancho = this.ancho * modificador_resolucion;
      int alto = this.alto * modificador_resolucion;

      var surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, ancho, alto );
      var cairo_context = new Cairo.Context ( surface );

      cairo_context.set_source_rgb ( 255, 255, 255 );
      cairo_context.paint ();

      cairo_context.set_source_rgb ( 0.1, 0.1, 0.1 );

      context = cairo_context;
    }

    private void borrar_paginas () {
      var paginas = this.get_children ();

      for ( uint i = 0; i < paginas.length (); i++ ) {
        var pagina = paginas.nth_data ( i );

        pagina.destroy ();
      }
    }

    private int calcular_margen_izquierdo ( int numero_pagina ) {
      int margen = 0;

      if ( this.documento.get_margenes_reflejados () == true ) {
        if ( ( numero_pagina % 2 ) == 0 ) {
          margen = documento.get_margen_izquierdo ();
        } else {
          margen = documento.get_margen_derecho ();
        }
      } else {
        margen = documento.get_margen_izquierdo ();
      }

      return margen;
    }

    private Cairo.Context dibujar_encabezado ( Array<Reemplazo> reemplazos, int margen_izquierdo, int modificador_resolucion  ) {
      int ancho = this.ancho * modificador_resolucion;
      int alto = this.alto * modificador_resolucion;

      Cairo.Surface encabezado_surface;
      Cairo.Surface texto_surface;
      Cairo.Surface imagenes_surface;
      Cairo.Surface imagen_surface;

      encabezado_surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, ancho, alto );
      var encabezado_context = new Cairo.Context ( encabezado_surface );

      if ( this.documento.get_encabezado () != null ) {

        Boga.Encabezado encabezado = this.documento.get_encabezado ();
        int margen_superior = this.documento.get_margen_superior () * modificador_resolucion;

        int encabezado_margen_superior = margen_superior - encabezado.get_alto () * modificador_resolucion;

        var layouts = encabezado.get_layouts ( reemplazos, modificador_resolucion );

        int encabezado_alto_dibujado = 0;

        texto_surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, ancho, alto );
        var texto_context = new Cairo.Context ( texto_surface );

        for ( int i = 0; i < layouts.length; i++ ) {
          texto_context.move_to ( margen_izquierdo, encabezado_margen_superior + encabezado_alto_dibujado );
          Pango.cairo_show_layout ( texto_context, layouts.index ( i ) );

          int alto_layout;
          layouts.index ( i ).get_pixel_size ( null, out alto_layout );
          encabezado_alto_dibujado += alto_layout;
        }

        var imagenes = encabezado.get_imagenes ();

        imagenes_surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, ancho, alto );
        var imagenes_context = new Cairo.Context ( imagenes_surface );

        for ( int i = 0; i < imagenes.length; i++ ) {
          var imagen = imagenes.index ( i );
          imagen_surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, ancho, alto );
          var imagen_context = new Cairo.Context ( imagen_surface );

          Gdk.cairo_set_source_pixbuf ( imagen_context, imagen.get_pixbuf ( modificador_resolucion ), margen_izquierdo + imagen.get_y (), encabezado_margen_superior + imagen.get_x () );
          imagen_context.paint ();

          imagenes_context.set_operator ( Cairo.Operator.OVER );
          imagenes_context.set_source_surface ( imagen_context.get_target (), 0, 0 );
          imagenes_context.paint ();
        }

        encabezado_context.set_operator ( Cairo.Operator.OVER );
        encabezado_context.set_source_surface ( texto_context.get_target (), 0, 0 );
        encabezado_context.paint ();
        encabezado_context.set_operator ( Cairo.Operator.OVER );
        encabezado_context.set_source_surface ( imagenes_context.get_target (), 0, 0 );
        encabezado_context.paint ();
      }
      return encabezado_context;
    }

    private Cairo.Context dibujar_recuadros ( int pagina_actual, int modificador_resolucion ) {
      int pagina_ancho = this.ancho * modificador_resolucion;
      int pagina_alto = this.alto * modificador_resolucion;

      Cairo.Surface recuadros_surface = new Cairo.ImageSurface ( Cairo.Format.ARGB32, pagina_ancho, pagina_alto );
      Cairo.Context recuadros_context = new Cairo.Context ( recuadros_surface );

      var recuadros = this.documento.get_recuadros ();

      for ( int i = 0; i < recuadros.length; i++ ) {
        var recuadro = recuadros.index ( i );

        if ( ( recuadro.get_pagina () -1 ) == pagina_actual ) {
          var recuadro_surface = new Cairo.ImageSurface  ( Cairo.Format.ARGB32, pagina_ancho, pagina_alto );
          var recuadro_context = new Cairo.Context ( recuadro_surface );

          var x = recuadro.get_x () * modificador_resolucion;
          var y = recuadro.get_y () * modificador_resolucion;
          var alto = recuadro.get_alto () * modificador_resolucion;
          var ancho = recuadro.get_ancho () * modificador_resolucion;

          recuadro_context.set_source_rgba ( 0, 0, 0, 1 );
          recuadro_context.set_line_width ( 2 * modificador_resolucion );

          recuadro_context.move_to ( x, y );

          recuadro_context.rel_line_to ( ancho, 0 );
          recuadro_context.rel_line_to ( 0, alto );
          recuadro_context.rel_line_to ( ( 0 - ancho ), 0 );
          recuadro_context.rel_line_to ( 0, ( 0 - alto ) );
          recuadro_context.close_path ();

          recuadro_context.stroke ();

          recuadros_context.set_operator ( Cairo.Operator.OVER );
          recuadros_context.set_source_surface ( recuadro_context.get_target (), 0, 0 );
          recuadros_context.paint ();
        }
      }

      return recuadros_context;
    }

    public void guardar_pdf ( string archivo, Array<Reemplazo> reemplazos ) {
      int modificador_resolucion = 4;

      this.crear_paginas ( reemplazos, modificador_resolucion, archivo );
    }
  }
}
