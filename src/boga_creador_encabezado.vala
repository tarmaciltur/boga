/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_creador_encabezado.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_creador_encabezado.ui" )]
  class CreadorEncabezado : Gtk.Grid {
    [GtkChild]
    private Gtk.Image image_button_revealer;
    [GtkChild]
    private Gtk.Revealer revealer;
    [GtkChild]
    private Gtk.Revealer revealer_opciones;
    [GtkChild]
    private Gtk.SpinButton spinbutton_alto;
    [GtkChild]
    private Gtk.ListBox listbox_secciones;
    [GtkChild]
    private Gtk.ListBox listbox_imagenes;
    private Boga.Nodo encabezado;
    private int cantidad_secciones;
    private int cantidad_imagenes;
    private int nivel;

    public signal void cambio ();

    public CreadorEncabezado ( int nivel ) {
      this.nivel = nivel;

      this.cantidad_secciones = 0;
      this.cantidad_imagenes = 0;
    }

    [GtkCallback]
    public void button_revealer_clicked () {
      if ( this.revealer.get_reveal_child () == false ) {
        this.image_button_revealer.set_from_icon_name ( "go-up-symbolic", Gtk.IconSize.BUTTON );
        this.revealer.set_reveal_child ( true );
        this.revealer_opciones.set_reveal_child ( true );
      } else {
        this.image_button_revealer.set_from_icon_name ( "go-down-symbolic", Gtk.IconSize.BUTTON );
        this.revealer.set_reveal_child ( false );
        this.revealer_opciones.set_reveal_child ( false );
      }
    }

    public Boga.Nodo get_nodo () {
      this.encabezado = new Boga.Nodo ( "encabezado", this.nivel );

      var nodo_alto = new Boga.Nodo ( "alto", this.nivel + 1 );
      if ( this.spinbutton_alto.text != "" ) {
        nodo_alto.set_contenido ( this.spinbutton_alto.text );
      } else {
        nodo_alto.set_contenido ( "0" );
      }
      this.encabezado.agregar_hijo ( nodo_alto );

      this.agregar_nodo_secciones ();
      this.agregar_nodo_imagenes ();

      return this.encabezado;
    }

    private void agregar_nodo_secciones () {
      int i = 0;
      Gtk.ListBoxRow row = this.listbox_secciones.get_row_at_index ( i );

      while ( row != null ) {
        Boga.CreadorSeccion seccion = row.get_child () as Boga.CreadorSeccion;
        this.encabezado.agregar_hijo ( seccion.get_nodo () );
        i++;
        row = this.listbox_secciones.get_row_at_index ( i );
      }
    }

    private void agregar_nodo_imagenes () {
      int i = 0;
      Gtk.ListBoxRow row = this.listbox_imagenes.get_row_at_index ( i );

      while ( row != null ) {
        Boga.CreadorImagen imagen = row.get_child () as Boga.CreadorImagen;
        this.encabezado.agregar_hijo ( imagen.get_nodo () );
        i++;
        row = this.listbox_imagenes.get_row_at_index ( i );
      }
    }

    [GtkCallback]
    private void actualizar () {
      this.cambio ();
    }

    [GtkCallback]
    private void button_agregar_seccion_clicked () {
      var seccion = new Boga.CreadorSeccion ( this.nivel + 1 );
      seccion.cambio.connect ( this.actualizar );

      this.listbox_secciones.set_visible ( true );
      this.listbox_secciones.insert ( seccion, -1 );
      this.sacar_foco_row ();

      this.cantidad_secciones++;
    }

    [GtkCallback]
    private void button_agregar_imagen_clicked () {
      var imagen = new Boga.CreadorImagen ( this.nivel + 1 );
      imagen.cambio.connect ( this.actualizar );

      this.listbox_imagenes.set_visible ( true );
      this.listbox_imagenes.insert ( imagen, -1 );
      this.sacar_foco_row ();

      this.cantidad_imagenes++;
    }

    private void sacar_foco_row () {
      int i = 0;
      Gtk.ListBoxRow row = this.listbox_secciones.get_row_at_index ( i );

      while ( row != null ) {
        row.set_can_focus ( false );
        i++;
        row = this.listbox_secciones.get_row_at_index ( i );
      }

      i = 0;
      row = this.listbox_imagenes.get_row_at_index ( i );

      while ( row != null ) {
        row.set_can_focus ( false );
        i++;
        row = this.listbox_imagenes.get_row_at_index ( i );
      }
    }

    public void set_encabezado ( Boga.Encabezado encabezado ) {
      this.spinbutton_alto.set_value ( encabezado.get_alto_en_mm () );

      var secciones = encabezado.get_secciones ();
      for ( int i = 0; i < secciones.length; i++ ) {
        this.button_agregar_seccion_clicked ();
        var creador_seccion = this.listbox_secciones.get_row_at_index ( this.cantidad_secciones -1 ).get_child () as Boga.CreadorSeccion;

        creador_seccion.set_seccion ( secciones.index ( i ) );
      }

      var imagenes = encabezado.get_imagenes ();
      for ( int i = 0; i < imagenes.length; i++ ) {
        this.button_agregar_imagen_clicked ();
        var creador_imagen = this.listbox_imagenes.get_row_at_index ( this.cantidad_imagenes -1 ).get_child () as Boga.CreadorImagen;

        creador_imagen.set_imagen ( imagenes.index ( i ) );
      }
    }
  }
}
