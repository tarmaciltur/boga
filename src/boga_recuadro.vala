/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_recuadro.vala
 *
 * Copyright 2019 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  public class Recuadro : Object {
    private int pagina;
    private int x;
    private int y;
    private int ancho;
    private int alto;
    private bool proporcion;

    public Recuadro ( string pagina,
                      string x,
                      string y,
                      string ancho,
                      string alto,
                      string proporcion ) {
      this.pagina = int.parse ( pagina );
      this.x = int.parse ( x );
      this.y = int.parse ( y );
      this.ancho = int.parse ( ancho );
      this.alto = int.parse ( alto );
      this.proporcion = bool.parse ( proporcion );
      if ( this.proporcion ) {
        this.alto = this.ancho;
      } else {
        this.alto = int.parse ( alto );
      }
    }

    public int get_pagina () {
      return this.pagina;
    }

    public int get_x () {
      return Boga.Utiles.mm_to_px ( this.x, ApplicationWindow.dpi );
    }

    public int get_y () {
      return Boga.Utiles.mm_to_px ( this.y, ApplicationWindow.dpi );
    }

    public int get_ancho () {
      return Boga.Utiles.mm_to_px ( this.ancho, ApplicationWindow.dpi );;
    }

    public int get_alto () {
      return Boga.Utiles.mm_to_px ( this.alto, ApplicationWindow.dpi );;
    }

    public int get_x_en_mm () {
      return this.x;
    }

    public int get_y_en_mm () {
      return this.y;
    }

    public int get_ancho_en_mm () {
      return this.ancho;
    }

    public int get_alto_en_mm () {
      return this.alto;
    }

    public bool get_proporcion () {
      return this.proporcion;
    }
  }
}
