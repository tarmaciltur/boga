/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_documento.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace Boga {
  public class Nodo : Object {
    private int nivel;
    private string nombre;
    private string contenido;
    private Array<Nodo> hijos;

    public Nodo ( string nombre, int nivel ) {
      this.nombre = nombre;
      this.nivel = nivel;

      this.contenido = "";
      this.hijos = new Array<Nodo> ();
    }

    public Nodo.desde_string ( string data, int nivel ) {
      int largo_tag = 0;
      if ( data[0] == '<' ) {
        largo_tag = data.index_of ( ">" );

        this( data.substring ( 1, largo_tag-1 ), nivel );
      }

      var texto_dentro_del_tag = data.substring ( largo_tag + 1, data.index_of ( "</" + this.nombre + ">" ) - (largo_tag + 1) );
      this.cargar_datos ( texto_dentro_del_tag );
    }

    public string get_nombre () {
      return this.nombre;
    }

    public string get_contenido () {
      return this.contenido;
    }

    public void set_contenido ( string contenido ) {
      this.contenido = contenido;
    }

    public void agregar_hijo ( Nodo hijo ) {
      this.hijos.append_val ( hijo );
    }

    public Array<Nodo> listar_hijos_nombre ( string nombre ) {
      Array<Nodo> retorno = new Array<Nodo> ();

      for ( int i = 0; i < this.hijos.length; i++ ) {
        var hijo = this.hijos.index ( i );

        if ( hijo.get_nombre () == nombre ) {
          retorno.append_val ( hijo );
        }
      }

      return retorno;
    }

    public Nodo primer_hijo_nombre ( string nombre ) {
      Nodo retorno = new Boga.Nodo ( nombre, this.nivel + 1 );

      for ( int i = 0; i < this.hijos.length; i++ ) {
        var hijo = this.hijos.index ( i );

        if ( hijo.get_nombre () == nombre ) {
          retorno = hijo;
          break;
        }
      }

      return retorno;
    }

    private void cargar_datos ( string data ) {
      int largo_tag = 0;
      string datos = data;

      while ( datos !=  "" ) {
        if ( datos[0] == '<' ) {
          largo_tag = datos.index_of ( ">" );

          string nombre_tag = datos.substring ( 1, largo_tag-1 );

          string finalizacion_tag = "</" + nombre_tag + ">";

          string texto_nodo_hijo = datos.substring ( 0, datos.index_of ( finalizacion_tag ) + finalizacion_tag.length );

          datos = datos.replace ( texto_nodo_hijo, "" );

          this.agregar_hijo ( new Boga.Nodo.desde_string ( texto_nodo_hijo, this.nivel + 1 ) );
        } else {
          this.contenido = datos.substring ( 0, datos.index_of ( "<" ) ).strip ();
          datos = datos.replace ( datos.substring ( 0, datos.index_of ( "<" ) ), "" );
        }
      }
    }

    public string to_string () {
      string indentacion = "";

      for ( int i = 0; i < this.nivel; i++ ) {
        indentacion += "  ";
      }

      string retorno = indentacion + "<" + this.nombre + ">";

      if ( this.hijos.length != 0 ) {
        retorno += "\n";
        for ( int i = 0; i < this.hijos.length; i++ ) {
          retorno += this.hijos.index ( i ).to_string ();
        }
      }

      if ( this.contenido != "" ) {
        retorno += this.contenido;
      }

      if ( retorno[retorno.length -1 ] != '\n' ) {
        indentacion = "";
      }

      retorno += indentacion + "</" + this.nombre + ">\n";

      return retorno;
    }
  }
}
