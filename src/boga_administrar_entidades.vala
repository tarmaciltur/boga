/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_administrar_entidades.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_administrar_entidades.ui" )]
  public class AdministrarEntidades : Gtk.Paned {
    [GtkChild]
    private Gtk.Stack stack_entidades_ver_editar;
    [GtkChild]
    private Gtk.ListBox listbox_lateral;
    [GtkChild]
    private Doctrina.ListBox listbox_ver_datos;
    [GtkChild]
    private Gtk.Grid grid_editar_datos;
    [GtkChild]
    private Gtk.Label label_eliminar_entidad;
    private string tabla;
    private string[] campos;
    private string[] campos_listbox_lateral;
    private string id_a_editar;
    private string id_a_eliminar;
    public signal void sin_entidades ();

    public AdministrarEntidades () {
      this.tabla = "";
      this.campos = {""};
      this.campos_listbox_lateral = {""};
      this.id_a_editar = "";
      this.id_a_eliminar = "";
    }

    protected void set_tabla ( string tabla ) {
      this.tabla = tabla;
      debug ( "tabla: " + this.tabla );
    }

    protected void set_campos ( string[] campos ) {
      this.campos = campos;
    }

    protected void set_campos_listbox_lateral ( string[] campos_listbox_lateral ) {
      this.campos_listbox_lateral = campos_listbox_lateral;
    }

    public bool cargar_entidades () {
      bool retorno = false;
      this.limpiar_entidades ();
      debug ( "tabla: " + this.tabla );
      var ids = ApplicationWindow.db.select ( this.tabla, "id", "" );

      for ( int i = 0; i < ids.length; i++ ) {
        debug ( "tabla: " + this.tabla );
        var row = new Boga.AdministrarEntidadesRow ( this.tabla, ids.index ( i ), this.campos_listbox_lateral );
        this.listbox_lateral.insert ( row, -1 );
      }

      if ( ids.length > 0 ) {
        retorno = true;
        var row = this.listbox_lateral.get_row_at_index ( 0 );
        this.listbox_lateral.select_row ( row );
        this.cargar_ver_entidad ( ids.index ( 0 ) );
      } else {
        debug ( "no hay entidades en la DB" );
        this.sin_entidades ();
      }

      return retorno;
    }

    private string campos_db () {
      string retorno = "";

      for ( int i = 0; i < this.campos.length; i++ ) {
        retorno += this.campos[i];

        if ( i < ( campos.length - 1 ) ) {
          retorno += ", ";
        }
      }

      return retorno;
    }

    private void limpiar_entidades () {
      var row = this.listbox_lateral.get_row_at_index ( 0 );

      while ( row != null ) {
        row.destroy ();
        row = this.listbox_lateral.get_row_at_index ( 0 );
      }
    }

    private void cargar_ver_entidad ( string id ) {
      var entidad = ApplicationWindow.db.select ( this.tabla,
                                                  this.campos_db (),
                                                  "WHERE id='" + id + "'" ).index ( 0 ).split ( "|" );

      this.listbox_ver_datos.limpiar ();
      for ( int i = 0; i < this.campos.length; i++ ) {
        var row = new Doctrina.RowLabel ( this.campos[i] );
        row.set_dato ( entidad[i] );
        this.listbox_ver_datos.insert ( row, -1 );
      }
    }

    [GtkCallback]
    private void listbox_lateral_row_activated () {
      var row = this.listbox_lateral.get_selected_row ().get_child () as Boga.AdministrarEntidadesRow;

      this.cargar_ver_entidad ( row.get_id () );
    }

    public void agregar_entidad () {
      this.id_a_editar = "";
      this.id_a_eliminar = "";

      this.limpiar_grid_editar_datos ();
      for ( int i = 0; i < this.campos.length; i++ ) {
        this.agregar_row_a_grid ( this.campos[i], "", i );
      }

      this.listbox_lateral.set_visible ( false );
      this.stack_entidades_ver_editar.set_visible_child_name ( "page_editar" );
    }

    public void ver_entidades () {
      this.listbox_lateral.set_visible ( true );
      this.stack_entidades_ver_editar.set_visible_child_name ( "page_ver" );
    }

    public bool agregar_entidad_aceptar () {
      bool validar = false;
      if ( this.id_a_eliminar != "" ) {
        validar = ApplicationWindow.db.del ( this.tabla, "WHERE id='" + this.id_a_eliminar + "'" );
      } else {
        Array<string> lista_datos = new Array<string> ();

        for ( int i = 0; i < this.campos.length; i++ ) {
          var entry = this.grid_editar_datos.get_child_at ( 1, i ) as Gtk.Entry;

          if ( entry.get_text () == "" ) {
            validar = false;
            break;
          }

          lista_datos.append_val ( entry.get_text () );
          validar = true;
        }

        if ( validar ) {
          if ( this.id_a_editar != "" ) {
            var datos = new Array<string> ();

            for (  int i = 0; i < this.campos.length; i++ ) {
              datos.append_val ( this.campos[i] + " = '" + lista_datos.index ( i ) + "'" );
            }

            ApplicationWindow.db.update ( this.tabla,
                                          datos,
                                          "id = '" + this.id_a_editar + "'" );
          } else {
            string datos = "";

            for (  int i = 0; i < this.campos.length; i++ ) {
              datos += "'" + lista_datos.index ( i ) + "'";

              if ( i < this.campos.length -1 ) {
                datos += ",";
              }
            }
            ApplicationWindow.db.insert ( this.tabla,
                                          this.campos_db (),
                                          datos );
          }
        }
      }

      return validar;
    }

    public void editar_entidad () {
      var row = this.listbox_lateral.get_selected_row ().get_child () as Boga.AdministrarEntidadesRow;

      this.id_a_eliminar = "";
      this.id_a_editar = row.get_id ();
      var entidad = ApplicationWindow.db.select ( this.tabla,
                                                  this.campos_db (),
                                                  "WHERE id='" + row.get_id () + "'" ).index ( 0 ).split ( "|" );

      this.limpiar_grid_editar_datos ();
      for ( int i = 0; i < this.campos.length; i++ ) {
        this.agregar_row_a_grid ( this.campos[i], entidad[i], i );
      }

      this.listbox_lateral.set_visible ( false );
      this.stack_entidades_ver_editar.set_visible_child_name ( "page_editar" );
    }

    public void eliminar_entidad () {
      var row = this.listbox_lateral.get_selected_row ().get_child () as Boga.AdministrarEntidadesRow;

      this.id_a_eliminar = row.get_id ();
      this.label_eliminar_entidad.set_label ( row.get_label () );

      this.listbox_lateral.set_visible ( false );
      this.stack_entidades_ver_editar.set_visible_child_name ( "page_eliminar" );
    }

    private void limpiar_grid_editar_datos () {
      var widgets = this.grid_editar_datos.get_children ();

      for ( uint i = 0; i < widgets.length (); i++ ) {
        var widget = widgets.nth_data ( i );

        widget.destroy ();
      }
    }

    private void agregar_row_a_grid ( string campo, string dato, int posicion ) {
      var label = new Gtk.Label ( Doctrina.Utiles.campo_a_nombre ( campo ) );
      label.set_visible ( true );
      label.set_halign ( Gtk.Align.START );
      this.grid_editar_datos.attach ( label, 0, posicion, 1, 1 );

      var entry = new Gtk.Entry ();
      entry.set_text ( dato );
      entry.set_width_chars ( 40 );
      entry.set_visible ( true );
      this.grid_editar_datos.attach ( entry, 1, posicion, 1, 1 );

    }
  }
}
