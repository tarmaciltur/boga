/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_ingresos.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_ingresos.ui" )]
  class Ingresos : Gtk.Grid {
    public Ingresos () {
    }

    public void limpiar_ingresos () {
      var ingresos = this.get_children ();

      for ( uint i = 0; i < ingresos.length (); i++ ) {
        var ingreso = ingresos.nth_data ( i );

        ingreso.destroy ();
      }
    }

    public void agregar_ingreso ( Boga.Ingreso ingreso, int pos ) {
      this.attach ( ingreso, 0, pos, 1, 1 );
    }

    public Array<Ingreso> get_ingresos () {
      Array<Ingreso> retorno = new Array<Ingreso> ();

      var ingresos = this.get_children ();
      for ( uint i = 0; i < ingresos.length (); i++ ) {
        var ingreso = ingresos.nth_data ( i ) as Boga.Ingreso;
        retorno.append_val ( ingreso );
      }

      return retorno;
    }

    public void alinear_columnas () {
      int ancho_widget;
      var ingresos = this.get_children ();
      int ancho_mayor = 0;

      for ( uint i = 0; i < ingresos.length (); i++ ) {
        var ingreso = ingresos.nth_data ( i ) as Boga.Ingreso;

        var widget = ingreso.get_child_at ( 0, 0 );

        widget.get_preferred_width ( out ancho_widget, null );

        if ( ancho_widget > ancho_mayor ) {
          ancho_mayor = ancho_widget;
        }
      }

      for ( uint i = 0; i < ingresos.length (); i++ ) {
        var ingreso = ingresos.nth_data ( i ) as Boga.Ingreso;

        var widget = ingreso.get_child_at ( 0, 0 );

        widget.get_preferred_width ( out ancho_widget, null );

        int margen = ancho_mayor - ancho_widget;

        widget.set_margin_end ( margen );
      }
    }
  }
}
