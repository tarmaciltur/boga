/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_ingreso.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_ingreso.ui" )]
  public class Ingreso : Gtk.Grid {
    [GtkChild]
    private Gtk.Label label_nombre;
    private string campo;
    private string separador_izq;
    private string separador_der;
    public signal void cambio_texto ();

    public Ingreso ( string campo, string separador_izq, string separador_der ) {
      this.campo = campo;
      this.separador_izq = separador_izq;
      this.separador_der = separador_der;

      this.label_nombre.set_label ( this.campo_a_nombre () );
    }

    public string get_campo () {
      return this.separador_izq + this.campo + this.separador_der;
    }

    public virtual string get_dato () {
      return "";
    }

    private string campo_a_nombre () {
      string retorno = this.campo.substring ( 0, this.campo.index_of ( ":", 0 ) );

      retorno = retorno.replace ( "_", " " );
      retorno = retorno.replace ( "-", " " );

      var palabras = retorno.split ( " " );

      retorno = "";
      for ( int i = 0; i < palabras.length; i++ ) {
        var primera_letra = palabras[i].substring ( 0, 1 );
        var resto_de_la_palabra = palabras[i].substring ( 1 );

        var palabra = primera_letra.up () + resto_de_la_palabra.down ();

        retorno += palabra + " ";
      }

      return retorno.strip ();
    }
  }
}
