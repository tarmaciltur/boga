/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_entrada.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  public class Entrada : Object {
    private string campo;
    private Boga.Entrada.Tipo tipo;

    public Entrada ( string campo, Boga.Entrada.Tipo tipo ) {
      this.campo = campo;
      this.tipo = tipo;
    }

    public Entrada.texto_corto ( string campo ){
      this.campo = campo;
      this.tipo = Boga.Entrada.Tipo.TEXTO_CORTO;
    }

    public Entrada.texto_largo ( string campo ){
      this.campo = campo;
      this.tipo = Boga.Entrada.Tipo.TEXTO_LARGO;
    }

    public Entrada.entidad ( string campo ){
      this.campo = campo;
      this.tipo = Boga.Entrada.Tipo.ENTIDAD;
    }

    public string get_campo () {
      return this.campo;
    }

    public Boga.Entrada.Tipo get_tipo () {
      return this.tipo;
    }

    public enum Tipo {
      TEXTO_CORTO,
      TEXTO_LARGO,
      ENTIDAD
    }
  }
}
