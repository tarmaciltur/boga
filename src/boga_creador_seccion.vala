/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* boga_creador_seccion.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Boga {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/boga/ui/boga_creador_seccion.ui" )]
  class CreadorSeccion : Gtk.Grid {
    [GtkChild]
    private Gtk.Image image_button_revealer;
    [GtkChild]
    private Gtk.Revealer revealer;
    [GtkChild]
    private Gtk.ListStore liststore_tipo_fuente;
    [GtkChild]
    private Gtk.ListStore liststore_interlineado;
    [GtkChild]
    private Gtk.ComboBox combobox_tipo_fuente;
    [GtkChild]
    private Gtk.Revealer revealer_button_formato;
    [GtkChild]
    private Gtk.SpinButton spinbutton_tamanio_fuente;
    [GtkChild]
    private Gtk.ToggleButton toggle_izquierda;
    [GtkChild]
    private Gtk.ToggleButton toggle_centro;
    [GtkChild]
    private Gtk.ToggleButton toggle_derecha;
    [GtkChild]
    private Gtk.ToggleButton toggle_justificado;
    [GtkChild]
    private Gtk.ToggleButton toggle_negrita;
    [GtkChild]
    private Gtk.SpinButton spinbutton_sangria;
    [GtkChild]
    private Gtk.ComboBox combobox_interlineado;
    [GtkChild]
    private Gtk.SpinButton spinbutton_desplazamiento_vertical;
    [GtkChild]
    private Gtk.SpinButton spinbutton_desplazamiento_horizontal;
    [GtkChild]
    private Gtk.TextView textview_texto;
    private int nivel;

    public signal void cambio ();

    public CreadorSeccion ( int nivel ) {
      this.nivel = nivel;
    }

    [GtkCallback]
    public void button_revealer_clicked () {
      if ( this.revealer.get_reveal_child () == false ) {
        this.image_button_revealer.set_from_icon_name ( "go-up-symbolic", Gtk.IconSize.BUTTON );
        this.revealer.set_reveal_child ( true );
        this.revealer_button_formato.set_reveal_child ( true );
      } else {
        this.image_button_revealer.set_from_icon_name ( "go-down-symbolic", Gtk.IconSize.BUTTON );
        this.revealer.set_reveal_child ( false );
        this.revealer_button_formato.set_reveal_child ( false );
      }
    }

    private string get_alineacion () {
      string retorno = "left";

      if ( this.toggle_centro.get_active () == true ) {
        retorno = "center";
      } else {
        if ( this.toggle_derecha.get_active () == true ) {
          retorno = "right";
        }
      }

      return retorno;
    }

    public Boga.Nodo get_nodo () {
      Gtk.TreeIter iter;
      var seccion = new Boga.Nodo ( "seccion", this.nivel );

      var nodo_fuente = new Boga.Nodo ( "fuente", this.nivel + 1 );
      string fuente = "Serif";
      if ( this.combobox_tipo_fuente.get_active_iter ( out iter ) ) {
        Value valor;
        this.liststore_tipo_fuente.get_value ( iter, 0, out valor );
        fuente = (string)valor;
      }
      fuente += " " + this.spinbutton_tamanio_fuente.text;
      nodo_fuente.set_contenido ( fuente );
      seccion.agregar_hijo ( nodo_fuente );

      var nodo_peso = new Boga.Nodo ( "peso", this.nivel + 1 );
      string peso = "normal";
      if ( this.toggle_negrita.get_active () == true ) {
        peso = "bold";
      }
      nodo_peso.set_contenido ( peso );
      seccion.agregar_hijo ( nodo_peso );

      var nodo_alineacion = new Boga.Nodo ( "alineacion", this.nivel + 1 );
      nodo_alineacion.set_contenido ( this.get_alineacion () );
      seccion.agregar_hijo ( nodo_alineacion );

      var nodo_justificado = new Boga.Nodo ( "justificado", this.nivel + 1 );
      nodo_justificado.set_contenido ( this.toggle_justificado.get_active ().to_string () );
      seccion.agregar_hijo ( nodo_justificado );

      var nodo_sangria = new Boga.Nodo ( "sangria", this.nivel + 1 );
      if ( this.spinbutton_sangria.text != "" ) {
        nodo_sangria.set_contenido ( this.spinbutton_sangria.text );
      } else {
        nodo_sangria.set_contenido ( "0" );
      }
      seccion.agregar_hijo ( nodo_sangria );

      var nodo_interlineado = new Boga.Nodo ( "interlineado", this.nivel + 1 );
      string interlineado = "0";
      if ( this.combobox_interlineado.get_active_iter ( out iter ) ) {
        Value valor;
        this.liststore_interlineado.get_value ( iter, 0, out valor );
        if ( (string)valor == "1,5" ) {
          interlineado= "3";
        } else {
          if ( (string)valor == "Doble" ) {
            interlineado = "6";
          }
        }
      }
      nodo_interlineado.set_contenido ( interlineado );
      seccion.agregar_hijo ( nodo_interlineado );

      var nodo_desplazamiento_vertical = new Boga.Nodo ( "desplazamiento-vertical", this.nivel + 1 );
      if ( this.spinbutton_desplazamiento_vertical.text != "" ) {
        nodo_desplazamiento_vertical.set_contenido ( this.spinbutton_desplazamiento_vertical.text );
      } else {
        nodo_desplazamiento_vertical.set_contenido ( "0" );
      }
      seccion.agregar_hijo ( nodo_desplazamiento_vertical );

      var nodo_desplazamiento_horizontal = new Boga.Nodo ( "desplazamiento-horizontal", this.nivel + 1 );
      if ( this.spinbutton_desplazamiento_horizontal.text != "" ) {
        nodo_desplazamiento_horizontal.set_contenido ( this.spinbutton_desplazamiento_horizontal.text );
      } else {
        nodo_desplazamiento_horizontal.set_contenido ( "0" );
      }
      seccion.agregar_hijo ( nodo_desplazamiento_horizontal );

      var nodo_texto = new Boga.Nodo ( "texto", this.nivel + 1 );
      if ( this.textview_texto.buffer.text != "" ) {
        nodo_texto.set_contenido ( this.textview_texto.buffer.text );
      } else {
        nodo_texto.set_contenido ( "" );
      }
      seccion.agregar_hijo ( nodo_texto );

      return seccion;
    }

    public void set_seccion ( Boga.Seccion seccion ) {
      this.textview_texto.buffer.set_text ( seccion.get_texto () );

      if ( seccion.get_justificado () ) {
        this.toggle_centro.set_active ( false );
        this.toggle_izquierda.set_active ( false );
        this.toggle_derecha.set_active ( false );
        this.toggle_justificado.set_active ( true );
      } else {
        if ( seccion.get_alineacion () == "center" ) {
          this.toggle_centro.set_active ( true );
          this.toggle_izquierda.set_active ( false );
          this.toggle_derecha.set_active ( false );
          this.toggle_justificado.set_active ( false );
        } else {
          if ( seccion.get_alineacion () == "right" ) {
            this.toggle_centro.set_active ( false );
            this.toggle_izquierda.set_active ( false );
            this.toggle_derecha.set_active ( true );
            this.toggle_justificado.set_active ( false );
          } else {
            this.toggle_centro.set_active ( false );
            this.toggle_izquierda.set_active ( true );
            this.toggle_derecha.set_active ( false );
            this.toggle_justificado.set_active ( false );
          }
        }
      }

      if ( seccion.get_peso () == "bold" ) {
        this.toggle_negrita.set_active ( true );
      } else {
        this.toggle_negrita.set_active ( false );
      }

      this.spinbutton_sangria.set_value ( seccion.get_sangria () );
      this.spinbutton_tamanio_fuente.set_value ( seccion.get_tamanio_fuente () );
      this.spinbutton_desplazamiento_vertical.set_value ( seccion.get_desplazamiento_vertical () );
      this.spinbutton_desplazamiento_horizontal.set_value ( seccion.get_desplazamiento_horizontal () );

      this.set_combobox_tipo_fuente ( seccion.get_fuente () );

      string interlineado = "Sencillo";
      if ( seccion.get_interlineado () == 3 ) {
        interlineado = "1,5";
      } else {
        if ( seccion.get_interlineado () == 6 ) {
          interlineado = "Doble";
        }
      }

      this.set_combobox_interlineado ( interlineado );
    }

    private void set_combobox_tipo_fuente ( string fuente ) {
      Gtk.TreeIter iter;
      bool coincide = false;
      Value valor;

      var model = this.combobox_tipo_fuente.get_model ();

      if ( model.get_iter_first ( out iter ) ) {
        do {
          model.get_value ( iter, 0, out valor );

          if ( fuente == (string)valor ) {
            coincide = true;
            break;
          }
        } while ( model.iter_next ( ref iter ) );

        if ( coincide ) {
          this.combobox_tipo_fuente.set_active_iter ( iter );
        }

      }
    }

    private void set_combobox_interlineado ( string interlineado ) {
      Gtk.TreeIter iter;
      bool coincide = false;
      Value valor;

      var model = this.combobox_interlineado.get_model ();

      if ( model.get_iter_first ( out iter ) ) {
        do {
          model.get_value ( iter, 0, out valor );
          if ( interlineado == (string)valor ) {
            coincide = true;
            break;
          }
        } while ( model.iter_next ( ref iter ) );

        if ( coincide ) {
          this.combobox_interlineado.set_active_iter ( iter );
        }

      }
    }

    [GtkCallback]
    private void actualizar () {
      this.cambio ();
    }

    [GtkCallback]
    private bool toggle_izquierda_press_event ( Gdk.EventButton evento ) {
      if ( this.toggle_izquierda.get_active () == false ) {
        this.toggle_izquierda.set_active ( true );
        this.toggle_centro.set_active ( false );
        this.toggle_derecha.set_active ( false );
        this.toggle_justificado.set_active ( false );
        this.cambio ();
      } else {
        this.toggle_izquierda.set_active ( true );
        this.toggle_justificado.set_active ( false );
        this.cambio ();
      }

      return true;
    }

    [GtkCallback]
    private bool toggle_centro_press_event ( Gdk.EventButton evento ) {
      if ( this.toggle_centro.get_active () == false ) {
        this.toggle_centro.set_active ( true );
        this.toggle_izquierda.set_active ( false );
        this.toggle_derecha.set_active ( false );
        this.toggle_justificado.set_active ( false );
        this.cambio ();
      } else {
        this.toggle_centro.set_active ( false );
        this.toggle_izquierda.set_active ( true );
        this.cambio ();
      }

      return true;
    }

    [GtkCallback]
    private bool toggle_derecha_press_event ( Gdk.EventButton evento ) {
      if ( this.toggle_derecha.get_active () == false ) {
        this.toggle_derecha.set_active ( true );
        this.toggle_centro.set_active ( false );
        this.toggle_izquierda.set_active ( false );
        this.toggle_justificado.set_active ( false );
        this.cambio ();
      } else {
        this.toggle_derecha.set_active ( false );
        this.toggle_izquierda.set_active ( true );
        this.cambio ();
      }

      return true;
    }

    [GtkCallback]
    private bool toggle_justificado_press_event ( Gdk.EventButton evento ) {
      if ( this.toggle_justificado.get_active () == false ) {
        this.toggle_justificado.set_active ( true );
        this.toggle_derecha.set_active ( false );
        this.toggle_centro.set_active ( false );
        this.toggle_izquierda.set_active ( false );
        this.cambio ();
      } else {
        this.toggle_justificado.set_active ( false );
        this.toggle_izquierda.set_active ( true );
        this.cambio ();
      }

      return true;
    }
  }
}
